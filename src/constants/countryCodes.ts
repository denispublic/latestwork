// List of valid country codes supported by our Backend

const COUNTRY_CODES =
[
  'AF', 'AL', 'AQ', 'DZ', 'AS', 'AD', 'AO', 'AG', 'AZ', 'AR', 'AU', 'AT', 'BS',
  'BH', 'BD', 'AM', 'BB', 'BE', 'BT', 'BO', 'BA', 'BW', 'BR', 'BZ', 'SB', 'BN',
  'BG', 'MM', 'BI', 'BY', 'KH', 'CM', 'CA', 'CV', 'CF', 'LK', 'TD', 'CL', 'CN',
  'CX', 'CC', 'CO', 'KM', 'CG', 'CD', 'CK', 'CR', 'HR', 'CY', 'CZ', 'BJ', 'DK',
  'DM', 'DO', 'EC', 'SV', 'GQ', 'ET', 'ER', 'EE', 'GS', 'FJ', 'FI', 'FR', 'PF',
  'TF', 'DJ', 'GA', 'GE', 'GM', 'DE', 'GH', 'KI', 'GR', 'GD', 'GU', 'GT', 'GN',
  'GY', 'HT', 'HM', 'VA', 'HN', 'HU', 'IS', 'IN', 'ID', 'IQ', 'IE', 'IL', 'IT',
  'CI', 'JM', 'JP', 'KZ', 'JO', 'KE', 'KR', 'KW', 'KG', 'LA', 'LB', 'LS', 'LV',
  'LR', 'LY', 'LI', 'LT', 'LU', 'MG', 'MW', 'MY', 'MV', 'ML', 'MT', 'MR', 'MU',
  'MX', 'MC', 'MN', 'MD', 'ME', 'MA', 'MZ', 'OM', 'NR', 'NP', 'NL', 'CW', 'SX',
  'BQ', 'NC', 'VU', 'NZ', 'NI', 'NE', 'NG', 'NU', 'NF', 'NO', 'MP', 'UM', 'FM',
  'MH', 'PW', 'PK', 'PA', 'PG', 'PY', 'PE', 'PH', 'PN', 'PL', 'PT', 'GW', 'TL',
  'QA', 'RO', 'RU', 'RW', 'SH', 'KN', 'LC', 'PM', 'VC', 'SM', 'ST', 'SA', 'SN',
  'RS', 'SC', 'SL', 'SG', 'SK', 'VN', 'SI', 'SO', 'ZA', 'ZW', 'ES', 'SR', 'SZ',
  'SE', 'CH', 'TJ', 'TH', 'TG', 'TK', 'TO', 'TT', 'AE', 'TN', 'TR', 'TM', 'TV',
  'UG', 'UA', 'MK', 'EG', 'GB', 'GG', 'JE', 'TZ', 'US', 'BF', 'UY', 'UZ', 'VE',
  'WF', 'WS', 'YE', 'ZM',
];

export default COUNTRY_CODES;
