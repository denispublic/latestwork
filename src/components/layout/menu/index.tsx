import {ReactNode, useState} from 'react';
import {useRouter} from 'next/router';
import {observer} from 'mobx-react-lite';
import {Menu as AntdMenu} from 'antd';
import styles from './style.module.scss';
import { pushAndKeepQueryParams } from '@/utils/router';

type ItemsGroup = {
  title: string;
  key: string;
  icon: ReactNode;
  path: string;
};

type MenuProps = {
  groups: ItemsGroup[];
  miniBar?: boolean;
};



const Menu: React.FC<MenuProps> = observer(({miniBar, groups, ...props}: MenuProps) => {
  const {Item} = AntdMenu;
  const router = useRouter();


  return (
    <AntdMenu
      className={styles.menu}
      mode='inline'
      {...props}
      onSelect={async e => {
        const path = groups[e.key].path;
        if (path) {
          await pushAndKeepQueryParams(router, path);
        }
      }}
    >
      {groups.map((menuItem, idx) => {
        return <Item key={idx}>{menuItem.icon}<span style={{marginLeft: 20}}>{menuItem.title}</span></Item>
      })}
      
    </AntdMenu>
  );
});


export default Menu;
