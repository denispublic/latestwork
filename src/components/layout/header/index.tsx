import {useEffect, useRef, useState} from 'react';
import {observer} from 'mobx-react-lite';
import moment from 'moment';
import styles from './style.module.scss';
import {useStore} from '@/store/root-store';
import {DatePicker, Tooltip, } from 'antd';
import {CaretDownOutlined} from '@ant-design/icons/lib';
import {useRouter} from 'next/router';
import {useTranslation} from 'next-i18next';
import {notification as antdNotification} from 'antd';
import classNames from 'classnames';
import {ChevronDown, ChevronUp} from 'react-feather';
// import COUNTRY_CODES from '@/constants/countryCodes';

const {RangePicker} = DatePicker;

const Header = observer(() => {
  const router = useRouter();
  const {t} = useTranslation('common');
  const [defaultDate, setDefaultDate] = useState([
    moment().subtract(2, 'years').subtract(1, 'days'),
    moment().subtract(1, 'days'),
  ]);
  const [dateRange, setDateRange] = useState('');
  const {
    homeStore: {getIsHome},
  } = useStore('');


  // Kill all notifications when changing page
  useEffect(() => {
    antdNotification.destroy();
  }, [router]);

  const {} = useStore('');

  const [isPickerOpen, setPickerOpen] = useState(false);

  const ActionsRangePicker = useRef(null);
  const [dateRangeTooltip, setDateRangeTooltip] = useState(false);
  const [dateRangeVisible, setDateRangeVisible] = useState(false);

  const isHomePage = location.pathname.match(/\/home.*/g);


  const dateRangePickerClickHandler = event => {
    event.stopPropagation();
    setDateRangeVisible(true);
  };

  const renderTooltip = () =>{
    return (
      <>
        <p>{t('select-custom-dates')}</p>
      </> 
    );
  };

  const renderDateRangePicker = (t:any) => (
    <div className={styles.ranger}>
      <div className={`${styles.rangerButton} ${isPickerOpen  ? styles.rangerButtonOpen : ''}`}
        ref={ActionsRangePicker}
      >
        <div onClick={e => dateRangePickerClickHandler(e)}>
          <Tooltip placement='bottom' title={renderTooltip} trigger='click'
            visible={dateRangeTooltip}
            overlayClassName={styles.upgradeTooltip}
          >
            <span style={{position: 'absolute', left: '20px', bottom: '-1px'}} className={styles.pickerHeading} >
              {t('date-range')}
            </span>
            <RangePicker
              onClick={e => e.stopPropagation()}
              picker={'date'}
              open={dateRangeVisible}
              className={styles.pickerHeader}
              style={{border: 'none', right: '6px', bottom: '1px', position: 'absolute', width: '100%', justifyContent: 'flex-end'}}
              dropdownClassName={`${dateRange === '1 month' ?
                styles.customPickerOneMonth :
                dateRange === '3 months' ? styles.customPickerThreeMonths :
                  dateRange === '6 months' ? styles.customPickerSixMonths :
                    dateRange === '1 year' ? styles.customPickerOneYear :
                      dateRange === '2 years' ? styles.customPickerTwoYears :
                        styles.customPicker}`}
              format={'DD MMM YYYY'}
              suffixIcon={isPickerOpen ? <ChevronUp/>:<ChevronDown/>}
              allowClear={false}
              ranges={{
                'Last 1 month': [
                  moment().subtract(1, 'months').subtract(1, 'days'),
                  moment().subtract(1, 'days'),
                ],
                'Last 3 months': [
                  moment().subtract(3, 'months').subtract(1, 'days'),
                  moment().subtract(1, 'days'),
                ],
                'Last 6 months': [
                  moment().subtract(6, 'months').subtract(1, 'days'),
                  moment().subtract(1, 'days'),
                ],
                'Last 1 year': [
                  moment().subtract(1, 'years').subtract(1, 'days'),
                  moment().subtract(1, 'days'),
                ],
                'Last 2 years': [
                  moment().subtract(2, 'years').subtract(1, 'days'),
                  moment().subtract(1, 'days'),
                ],
              }}
              value={[
                defaultDate[0],
                defaultDate[1],
              ]}
              separator={'-'}
              allowEmpty={[false, false]}
              onCalendarChange={(dates: Array<moment.Moment>, [currentPeriodStart, currentPeriodEnd]: [string, string]) => {
                const dateRange = dates && dates[1].diff(dates[0], 'days');
                let periodStatus = '';
                if ([28, 29, 30, 31].includes(dateRange)) {
                  setDateRange('1 month');
                  periodStatus = '1 month';
                }
                if ([87, 88, 89, 90, 91, 92].includes(dateRange)) {
                  setDateRange('3 months');
                  periodStatus = '3 months';
                }
                if ([180, 181, 182, 183, 184].includes(dateRange)) {
                  setDateRange('6 months');
                  periodStatus = '6 months';
                }
                if ([364, 365, 366, 367].includes(dateRange)) {
                  setDateRange('1 year');
                  periodStatus = '1 year';
                }
                if ([728, 729, 730, 731, 732].includes(dateRange)) {
                  setDateRange('2 years');
                  periodStatus = '2 years';
                }
                
              }}
              disabledDate={currentDate => currentDate >= moment().subtract(1, 'days')}
              onOpenChange={(open: boolean) => {
                setPickerOpen(open);
              }}
            />
          </Tooltip>
        </div>
        <span className={styles.rangerCaret} style={{position: 'absolute', marginLeft: '276px'}} onClick={e => dateRangePickerClickHandler(e)}>
          <CaretDownOutlined />
        </span>
      </div>

    </div>
  );

  return (
    <div className={classNames(styles.header)}>
      {isHomePage && (
        <div className={styles['header__gsc-menu']}>
          <div className={styles.header__left}>
            {renderDateRangePicker(t)}
          </div>
        </div>
      )}
    </div>
  );
});

export default Header;
