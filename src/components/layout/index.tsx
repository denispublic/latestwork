import React, {PropsWithChildren, useState} from 'react';
import {Layout as AntdLayout} from 'antd';
import {observer} from 'mobx-react-lite';
import Menu from './menu';
import {useRouter} from 'next/router';
import styled from 'styled-components';
import {RootStoreType} from '@/store/root-store';
import styles from './style.module.scss';
import {routes} from '@/utils/const';
import {ArrowsAltOutlined, ShrinkOutlined, HomeOutlined, FormOutlined } from '@ant-design/icons';

type PrivateLayoutProps = PropsWithChildren<{
  store: RootStoreType;
}>;

const PrivateLayout: React.FC<PrivateLayoutProps> = ({children}) => {
  return <div><AntdLayout className={styles.layout}>{children}</AntdLayout></div>;
};



type PublicLayoutProps = {
  children: React.ReactNode;
};

const PublicLayout: React.FC<PublicLayoutProps> =  observer<PublicLayoutProps>(({children}) => {
  const [isDocked, setIsDocked] = useState<boolean>(false);

    // menu items to be used in our side menu
    const menuItems = [
            {
              title: 'Draft JS Editor',
              key: 'draft-js-editor',
              icon: <HomeOutlined onClick={() => {}} />,
              path: `/${routes.draftJsEditor}`,

            },
            {
              title: 'Custom Inputs',
              key: 'custom-inputs',
              icon: <FormOutlined onClick={() => {}} />,
              path: `/${routes.commonComponents}`,

            },
          ];


    return (
      <>
        <div style={{display: 'flex'}}>
          <SideBar isDocked={isDocked}>
            {isDocked ? <CustomExpandIcon onClick={() => setIsDocked(false)}/> : <CustomCollapsedIcon onClick={() => setIsDocked(true)}/>}
            <Menu groups={menuItems}/>
          </SideBar>
          {children}
        </div>
      </>
    );
  },
);

type LayoutProps = PropsWithChildren<{
  pathname: string;
  store: RootStoreType;
}>;

const Layout = observer(({pathname, store, children}: LayoutProps) => {
  let path = pathname;
  if (!pathname) {
    const router = useRouter();
    path = router.pathname;
  }

  // indexing all public pages (visible to all public users)
  const publicPages = [
    '/draftJsEditor',
    '/commonComponents',
    '/',
  ];

  // We can dinamically show different layout for different routes
  return publicPages.includes(path) ? (
    <PublicLayout>{children}</PublicLayout>
  ) : (
    <PrivateLayout store={store}>{children}</PrivateLayout>
  );
});

const CustomExpandIcon = styled(ArrowsAltOutlined)`
  color: #fff;
  margin: 20px 0 10px 20px;
  font-size: 20px;
`

const CustomCollapsedIcon = styled(ShrinkOutlined)`
  color: #fff;
  margin: 20px 0 10px 20px;
  font-size: 20px;
`

const SideBar = styled.div<{isDocked: boolean}>`
  background-color: #24262A;
  z-index: 3;
  overflow-x: hidden;
  width: ${p => p.isDocked ? '260px' : '58px'};
  height: 100vh;
  transition: width 0.2s;
  border-right: 1px solid #323438;
  height: 100vh;

  &:hover {
    overflow-y: auto;
  }

  &::-webkit-scrollbar {
    width: 3px;
    height: 3px;
  }

  &::-webkit-scrollbar-thumb {
    background: #4E5156;
  }

  &::-webkit-scrollbar-track {
    background: #282a2e;
  }
`;


export default Layout;
