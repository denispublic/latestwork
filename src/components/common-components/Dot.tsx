import React from 'react';
import styled from 'styled-components';
interface DotProps {
  color?: string;
  size?: string;
}
export const Dot = ({color = '', size = '10px', ...props}: DotProps) => {
  return <DotStyled color={color} size={size} {...props}></DotStyled>;
};
const DotStyled = styled.div<DotProps>`
  border-radius: 50%;
  height: ${props => props.size};
  margin-right: 5px;
  width:  ${props => props.size};
  background-color: ${props => props.color};
`;
