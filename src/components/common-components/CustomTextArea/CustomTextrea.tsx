import {useEffect, useRef} from 'react';
import styles from './style.module.scss';
import classnames from 'classnames';

export const CustomTextarea = ({...props}) => {
  const textAreaRef = useRef(null);
  const numberAreaRef = useRef(null);

  const inputChanged = objTxt=> {
    const objRownr = numberAreaRef.current;
    const cntline = countLines(objTxt.value);
    const tmpArr = objRownr.value.split('\n');
    const cntlineOld = parseInt(tmpArr[tmpArr.length - 1], 10);
    if (cntline != cntlineOld) {
      objRownr.cols = cntline.toString().length;
      populateRownr(objRownr, cntline);
    }
    if (props?.autoSize && objTxt.scrollHeight < 500) {
      objTxt.style.height = '38px';
      objRownr.style.height = '38px';
      objTxt.style.height = objTxt.scrollHeight + 'px';
      objRownr.style.height = objTxt.scrollHeight + 'px';
    }
  };
  const populateRownr =(obj, cntline)=> {
    let tmpstr = '';
    for ( let i = 1; i <= cntline; i++) {
      tmpstr = tmpstr + i.toString() + '.\n';
    }
    obj.value = tmpstr;
  };
  const countLines = txt=> {
    if (txt == '') {
      return 0;
    }
    return txt.split('\n').length;
  };
  const scrollChanged = objTxt=> {
    const objRownr = numberAreaRef.current;
    scrollsync(objTxt, objRownr);
  };
  const scrollsync =(obj1, obj2)=> {
    obj2.scrollTop = obj1.scrollTop;
  };
  useEffect(()=>{
    inputChanged(textAreaRef.current);
  });

  if (typeof window !== 'undefined'){
    const ele = document.getElementById('message');
    ele?.addEventListener('keydown', function(e) {
      const keyCode = e.which || e.keyCode;
      if (keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
      }
    });
  }



  return (<>
      <div className={classnames(
        {
          [styles.light]: props.variant === 'light',
          [styles.dark]: props.variant === 'dark',
          [styles.transparent]: props.variant === 'transparent',
        },
        styles.customTextarea,
      )}>
        <textarea id='message'
          ref={numberAreaRef} style={{minHeight: props?.minHeight}} className={styles.rownr} rows={props?.rows ? props?.rows : 8} cols={3} value={1} readOnly = {true} ></textarea>
        <span>
          <textarea
            id='message'
            {...props}
            style={{minHeight: props?.minHeight, width: props?.width}}
            className={styles.txt}
            rows={props?.rows ? props?.rows : 8}
            cols={props?.cols ? props?.cols : 30}
            autoComplete='off' autoCorrect='off' autoCapitalize='off' spellCheck='false'
            ref={textAreaRef}
            onScroll={() => {
              scrollChanged(textAreaRef.current);
            }}
            onInput={()=>{
              inputChanged(textAreaRef.current);
            }}
          >
            {props.value}
          </textarea>
        </span>
      </div> </>)
};

