import React from 'react';
import {countries, Country} from 'countries-list';
import COUNTRY_CODES from '@/constants/countryCodes';
import {Globe} from 'react-feather';
import {Select as AntdSelect, SelectProps} from 'antd';
import styled from 'styled-components';
import {COLORS} from './colors';
import {useTranslation} from 'next-i18next';
const {Option} = AntdSelect;

interface Props extends SelectProps<any>{
  variant?: 'light' | 'dark';
}

const countriesList = COUNTRY_CODES.map(code => {
  const country: Country = countries[code];
  return ({
    code: code,
    name: country.name,
    emoji: country.emoji,
  });
}).sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));

export const CountryDropdown = ({variant = 'light', ...props}: Props) => {
  const {t} = useTranslation('common');

  return (
    <StyledSelect
      {...props}
      defaultValue='US'
      variant={variant}
      showSearch
      dropdownStyle={{backgroundColor: variant === 'dark' ? COLORS.grayDark : ''}}
      filterOption={(input, option) =>
        option.country.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <Option style={{backgroundColor: variant === 'dark' ? COLORS.grayDark : COLORS.white}} value={''} country={'WorldWide'}><GlobeContainer variant={variant} ><StyledGlobe />{t('worldwide')}</GlobeContainer></Option>
      {
        countriesList.map( item => {
          return <Option style={{backgroundColor: variant === 'dark' ? COLORS.grayDark : COLORS.white}} key={item.code} country={item.name} value={item.code}><OptionContent variant={variant}>{item.emoji} {item.name}</OptionContent></Option>;
        })
      }
    </StyledSelect>
  );
};
const variantColors = {
  light: {
    color: COLORS.black,
    backgroundColor: COLORS.white,
    borderColor: COLORS.grayDark,

  },

  dark: {
    color: COLORS.white,
    backgroundColor: COLORS.grayDark,
    borderColor: COLORS.grayDark,
  }};
const StyledSelect = styled(AntdSelect)<Props>`
  color:${ p => variantColors[p.variant].color};
  .ant-select-selector {
    max-width:180px !important;
    min-width:180px !important;
    border-radius: 5px !important;
    background-color: ${ p => variantColors[p.variant].backgroundColor} !important;
    border-color: ${ p => variantColors[p.variant].borderColor} !important;
    padding: 6px 15px !important;
    height: 44px !important;
    cursor: pointer !important;
    line-height: unset !important;
  }
  .ant-select-selection-search {
    padding: 6px 0;
  }

 .ant-select-arrow {
  color:${ p => variantColors[p.variant].color};
  }
  .ant-select-item-option-selected:not(.ant-select-item-option-disabled){
    background-color: ${ p => variantColors[p.variant].backgroundColor} !important;
    border-color: ${ p => variantColors[p.variant].borderColor} !important
  }
`;

const GlobeContainer=styled.div<{variant: string}>`
{
  color:${ p => variantColors[p.variant].color};
  background-color: ${ p => variantColors[p.variant].backgroundColor} !important;
  padding:0px;
  align-items: center;
  display: flex;
}

`;
const StyledGlobe=styled(Globe)`
  padding-right: 3px;
  width: 20px;
`;

const OptionContent = styled.div<{variant: string}>`
  background-color: ${ p => variantColors[p.variant].backgroundColor} !important;
  border-color: ${ p => variantColors[p.variant].borderColor} !important;
  color:${ p => variantColors[p.variant].color} !important;
  max-width: 140px;
  height: 100%;
  overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
`;
