import {
  Button as AntdButton,
  ButtonProps as AntdButtonProps,
} from 'antd';
import styled from 'styled-components';
import {COLORS} from './colors';

interface ButtonProps extends AntdButtonProps {
  color?: 'purple' | 'white' | 'tableGray' | 'textGrayLight' | 'textGrayDark' | 'green' | 'red';
  variant?: 'solid' | 'ghost';
  mode?: 'light' | 'dark';
  background?: string;
  textColor?: string;
}

// Figma:
// https://www.figma.com/file/tdvnkn6V30KvcYXqs0lJ6Y/Tools-Dashboard?node-id=7575%3A2564

export const Button = ({color = 'purple', variant = 'solid', mode='light', background = '', textColor = '', ...props}: ButtonProps) => {
  return (
    <ButtonStyled color={color} variant={variant} background={background} mode={mode} textColor={textColor} {...props}></ButtonStyled>
  );
};

const ButtonStyled = styled(AntdButton)<ButtonProps>`
  background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}1C` : `${COLORS[p.color]}` };
  border-radius: 5px;
  border: ${p => p.mode === 'dark' ? `1px solid ${COLORS[p.color]}` : '1px solid transparent'};
  padding: 7px 20px;
  color: ${p => p.mode === 'dark' || p.variant === 'solid' ? '#fff' : p.textColor ? `${p.textColor}` : COLORS[p.color]};
  // border-color: ${p => p.background ? `${p.background}` : p.color === 'white' ? COLORS.textGrayLight : COLORS[p.color]};
  font-size: 14px;
  width:auto;
  height: auto;
  margin:0px 10px;

  :hover {
    background: ${p => p.variant === 'solid' && `linear-gradient(0deg, rgba(0, 0, 0, 0.18), rgba(0, 0, 0, 0.18)),${p.background ? `${p.background}` : COLORS[p.color]} !important`};
    background-color: ${p => `${COLORS[p.color]}2E`};
    border: ${p => p.mode === 'dark' ? `1px solid ${COLORS[p.color]}` : '1px solid transparent'};
    // background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}2E` : COLORS[p.color] };
    // border-color: ${p => p.background ? `${p.background}` : COLORS[p.color]};
      color: ${p => p.mode === 'dark' || p.variant === 'solid' ? '#fff' : p.textColor ? `${p.textColor}` : COLORS[p.color]};
  }

  :active {
    background: ${p => p.variant === 'solid' && `linear-gradient(0deg, rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25)),${p.background ? `${p.background}` : COLORS[p.color]} !important`};
    background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}40` : COLORS[p.color] };
    border: ${p => p.mode === 'dark' ? `1px solid ${COLORS[p.color]}` : '1px solid transparent'};
    // border-color: ${p => p.background ? `${p.background}` : COLORS[p.color]};
      color: ${p => p.mode === 'dark' || p.variant === 'solid' ? '#fff' : p.textColor ? `${p.textColor}` : COLORS[p.color]};
  }
  :disabled{
    background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}1C` : COLORS[p.color] };
    border-radius: 5px;
    // padding: 10px 20px;
    color: ${p => p.textColor ? `${p.textColor}` : p.color === 'tableGray' || p.color === 'textGrayLight' || p.color === 'white' ? COLORS.black : COLORS.white};
    // border-color: ${p => p.background ? `${p.background}` : p.color === 'white' ? COLORS.textGrayLight : COLORS[p.color]};
    opacity: 50%;
    :hover {
      background: linear-gradient(0deg, rgba(0, 0, 0, 0.17), rgba(0, 0, 0, 0.17)), ${p => p.background ? `${p.background}` : COLORS[p.color]};
      background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}2E` : COLORS[p.color] };
      border-color: ${p => p.background ? `${p.background}` : COLORS[p.color]};
        color: ${p => p.mode === 'dark' || p.variant === 'solid' ? '#fff' : p.textColor ? `${p.textColor}` : COLORS[p.color]};
    }
  }

  :focus {
    background: ${p => p.variant === 'solid' && `linear-gradient(0deg, rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25)),${p.background ? `${p.background}` : COLORS[p.color]} !important`};
    background-color: ${p => p.background ? `${p.background}` : p.variant === 'ghost' ? `${COLORS[p.color]}40` : COLORS[p.color] };
    border: ${p => p.mode === 'dark' ? `1px solid ${COLORS[p.color]}` : '1px solid transparent'};
    // border-color: ${p => p.background ? `${p.background}` : COLORS[p.color]};
    color: ${p => p.mode === 'dark' || p.variant === 'solid' ? '#fff' : p.textColor ? `${p.textColor}` : COLORS[p.color]};
  }
`;

