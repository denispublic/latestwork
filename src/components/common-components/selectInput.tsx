import {Select} from 'antd';
import styled from 'styled-components';
import {COLORS} from './colors';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {CheckOutlined} from '@ant-design/icons';
interface SelectProps {
  variant?: 'light' | 'dark';
  checkIcon?:boolean;
  fieldNames:any;
}

export const SelectInput = ({...props}: SelectProps) => {
  return (
    <SelectStyled
      dropdownStyle={{backgroundColor: props.variant === 'dark' ? COLORS.grayDark : ''}}
      menuItemSelectedIcon={props?.checkIcon && <CheckOutlined style={{color: '#2AC155'}}/>}
      variant={props.variant}
      {...props}
    >
      {props?.fieldNames?.map(title => (
        <Select.Option key={title} value={title}><OptionContent variant={props.variant}>{title}</OptionContent> </Select.Option>))}    </SelectStyled>
  );
};

const variantColors = {
  light: {
    color: COLORS.black,
    background: COLORS.white,
    borderColor: COLORS.white,
  },
  dark: {
    color: COLORS.white,
    background: COLORS.grayDark,
    borderColor: COLORS.grayDark,
  },
};

const SelectStyled = styled(Select)<SelectProps>`
  .ant-select-selector {
    background-color: ${p => variantColors[p.variant].background} !important;
  }
  .ant-select-selection-item {
    color: ${p => variantColors[p.variant].color || variantColors['white'].color};
  }
  .ant-select-item-option-content {
    color: ${p => variantColors[p.variant].color || variantColors['white'].color};
  }
  .global(.ant-select-item ant-select-item-option ant-select-item-option-selected) {
    color: purple !important;
  }
  .ant-select-arrow{
    color: ${p => variantColors[p.variant].color || variantColors['white'].color} ;
  }
  .ant-select-dropdown{
    background-color: ${p => variantColors[p.variant].background} !important;
    color: ${p => variantColors[p.variant].color || variantColors['white'].color} ;


  }
`;
const OptionContent = styled.span<{variant:string}>`
  color:${ p => variantColors[p.variant].color} !important;
`;
