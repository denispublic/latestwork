export enum COLORS {
  purple = '#7f4ead',
  white = '#fff',
  black = '#121212',
  green = '#1FAC47',
  red = '#F44343',
  gray = '#A3A4A4',
  grayLight = '#3c3e42',
  grayDark = '#2d2f34',
  tableGray = '#F9F9FB',
  textGrayLight = '#E8E8E8',
  textGrayDark = '#4E5156',
  yellow ='#F1AA3E',
}
