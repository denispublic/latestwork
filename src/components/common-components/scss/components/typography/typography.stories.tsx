import {Story, Meta} from '@storybook/react';
import {Typography, TypographyProps} from '.';

export default {
  title: 'Typography',
  component: Typography,
} as Meta;

const Template: Story<TypographyProps> = args => <Typography {...args}>Content</Typography>;

export const h1 = Template.bind({});
h1.args = {tag: 'h1'} as TypographyProps;

export const h2 = Template.bind({});
h2.args = {tag: 'h2'} as TypographyProps;

export const h3 = Template.bind({});
h3.args = {tag: 'h3'} as TypographyProps;

export const h4 = Template.bind({});
h4.args = {tag: 'h4'} as TypographyProps;

export const h5 = Template.bind({});
h5.args = {tag: 'h5'} as TypographyProps;

export const h6 = Template.bind({});
h6.args = {tag: 'h6'} as TypographyProps;

export const p = Template.bind({});
p.args = {tag: 'p'} as TypographyProps;

export const label = Template.bind({});
label.args = {tag: 'label'} as TypographyProps;

export const strong = Template.bind({});
strong.args = {tag: 'strong'} as TypographyProps;

export const span = Template.bind({});
span.args = {tag: 'span'} as TypographyProps;
