import {Collapse} from 'antd';
import {ReactNode} from 'react';
import styled from 'styled-components';
import {COLORS} from './colors';
import {CaretDownOutlined} from '@ant-design/icons';

/*
* ACCORDION WRAPPER START
*
* to disable make sure to pass it as a custom prop
*/

type AntAccordionProps = React.ComponentProps<typeof Collapse>;

interface AccordionProps extends AntAccordionProps {
  children: ReactNode;
  collapse?: boolean;
}

export const Accordion = ({children, collapse = true, ...props}: AccordionProps) => {
  const customIconPosition = props.expandIconPosition ?? 'right';

  const CustomExpandIcon = ({isActive}) => props.expandIcon ?? <CaretDownStyled  isActive={isActive}/>;

  return <CollapseStyled
    expandIcon={CustomExpandIcon}
    expandIconPosition={customIconPosition}
    collapsible={collapse ? props.collapsible : 'disabled'}
    {...props} >
    {children}
  </CollapseStyled>;
};

const CollapseStyled = styled(Collapse)`
  width: 100%;
  border: 0px;
  background-color: transparent !important;

  .ant-collapse {
    background-color: transparent !important;
  }

  .ant-collapse-header {
    padding: 2px 40px 2px 16px !important;
    background-color: ${COLORS.grayLight};
    color: ${COLORS.white} !important;
    font-size: 13px;
  }

  .ant-collapse-content {
    background-color:  ${COLORS.grayDark};
    border-top: 0px;
  }

  .ant-collapse-item {
    border-bottom: 0px;
  }
`;

const CaretDownStyled = styled(CaretDownOutlined)<{isActive: boolean}>`
  font-size: 16px;
  transition: all .3s ease;
  top: 7px !important;
  transform: rotate(${p => p.isActive ? '180deg' : '0deg'}) !important;
  color: ${COLORS.gray};
  margin-right: 5px !important;
`;

/*
* ACCORDION WRAPPER END
*/

/** ********************************************/

/*
* SINGLE PANEL START
*/

const {Panel} = Collapse;

type AntPanelProps = React.ComponentProps<typeof Panel>;

interface PanelProps extends AntPanelProps {
  children: ReactNode;
}

export const AccordionPanel = ({children, ...props}: PanelProps) => {
  return <PanelStyled {...props}>
    {children}
  </PanelStyled>;
};

const PanelStyled = styled(Panel)`
  width: 100%;

  .ant-collapse-content-box {
    padding: 6px 16px 20px 6px;
  }
`;

/*
* SINGLE PANEL END
*/

