import React, {useState, useEffect, useRef, useCallback} from 'react';
import {RadioButton} from './radioButton';
import {Input} from './Input';
import styled from 'styled-components';
import {Button} from './Button';
import {useTranslation} from 'next-i18next';
import {CaretDownOutlined} from '@ant-design/icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faClose} from '@fortawesome/pro-regular-svg-icons';

interface Props {
  type?: string;
  add?:boolean;
  filtersLength?:number;
  filterId?: number;
  filterLabel?: string;
  filterName?: string;
  activeFilter?:any;
  onClose?: ()=>void;
  removeItem?: ()=>void;
  onChange?: (filter:any)=>void;
  className?: string;
  initialFrom?: string;
  initialTo?: string;
}

const FilterInput: React.FC<Props> = ({removeItem, type, filterId, filterLabel, filterName, activeFilter, onChange, className, initialFrom, initialTo}) => {
  const [value, setValue] = useState('between');
  const [to, setTo] = useState('');
  const [from, setFrom] = useState('');
  const [filterOpen, setFilterOpen] = useState(false);
  const ref = useRef(null);
  const {t} = useTranslation('common');

  useEffect(() => {
    if (initialFrom) {
      setFrom(initialFrom);
    }
    if (initialTo) {
      setTo(initialTo);
    }
    if (type) {
      setValue(type);
    }
  }, []);

  const onChangeValue = useCallback(
    e => setValue(e.target.value),
    [],
  );

  const createOrUpdateFilter = e =>{
    e.preventDefault();
    emitOnChanges(from, to);
    setFilterOpen(false);
  };

  const onCancel = () => {
    if (initialFrom || initialTo) {
      removeItem();
    }
    setFilterOpen(false);
  };

  const emitOnChanges = (from: string, to: string) => {
    if (typeof onChange === 'function') {
      onChange({
        id: filterId,
        header: filterLabel,
        name: filterName,
        type: value,
        from: !from ? '0' : from,
        to,
        active: true,
      });
    }
  };

  const handleClickOutside = event => {
    const getBtnId = event?.target?.id == `openFilterButton-${filterLabel}` || event?.target?.offsetParent?.id == `openFilterButton-${filterLabel}`;
    if (ref.current && !ref.current.contains(event.target) && !getBtnId) {
      setFilterOpen(false);
    }
  };

  const onRemoveFilter = e =>{
    e.stopPropagation();
    setFrom('');
    setTo('');
    if (typeof onChange === 'function') {
      onChange({
        id: filterId,
        header: filterLabel,
        name: filterName,
        type: value,
        active: false,
      });
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, []);

  const disablingButton = () =>{
    if (value === 'between' && from && !to ) return false;
    if (value === 'between' && !from && to ) return false;
    if (value === 'between' && (!from && !to) ) return true;
    if (value === 'between' && from && to && (from > to) ) return true;
    if (value ==='Greater than' && !from) return true;
    if (value === 'less than' && !to) return true;
    if ((initialFrom || initialTo) && (to ===initialTo && from ===initialFrom)) return true;
    return false;
  };

  useEffect(() => {
    if (!type || !filterOpen) {
      setTo('');
      setFrom('');
    }
  }, [value, filterOpen]);


  return <>
    <div style={{position: 'relative'}} className={className}>
      <Button id={`openFilterButton-${filterLabel}`} color='tableGray' variant='solid' style={{margin: '5px', border: '1px solid #E8E8E8'}} onClick={() => setFilterOpen(!filterOpen)}>
        {filterLabel}{activeFilter?.name == filterName && activeFilter?.active ?
          <span style={{marginLeft: '7px'}} onClick={onRemoveFilter}><FontAwesomeIcon icon={faClose} style={{color: '#121212', fontSize: '13px'}}/></span> : <CaretDownOutlined style={{marginLeft: '10px'}} />}</Button>
      {filterOpen ?
        <MainConatiner ref={ref}>
          <RadioContainer>
            <div style={{marginBottom: '14px'}}> <RadioButton onChange={onChangeValue} checked={value == 'between'} value={'between'} label='between' />
              {value=='between' &&
                 <div style={{display: 'flex', alignItems: 'center'}}>
                   <Input type='num' onChange={e=> setFrom(e.target.value.replace(/\D/g, ''))} variant='gray'/>
                   <span style={{margin: '10px 5px 0 5px'}}>and</span>
                   <Input onChange={e=> setTo(e.target.value.replace(/\D/g, ''))} variant='gray'/>
                 </div>}
            </div>
            <div style={{marginBottom: '14px'}}><RadioButton value={'Greater than'} onChange={onChangeValue} checked={value == 'Greater than'} label='greater than'/>
              {value=='Greater than' &&
                <Input onChange={e=> setFrom(e.target.value.replace(/\D/g, ''))} variant='gray'/>}
            </div>
            <div style={{marginBottom: '14px'}}><RadioButton value={'less than'} onChange={onChangeValue} checked={value == 'less than'} label='less than'/>
              {value=='less than' &&
                <Input onChange={e=> setTo(e.target.value.replace(/\D/g, ''))} variant='gray'/>}
            </div>
          </RadioContainer>
          <ButtonConatiner><Button style={{marginRight: '10px'}} color='tableGray' variant='solid' onClick={onCancel}>
            {(initialFrom || initialTo)?t('remove'): t('cancel')}
          </Button>
          <Button color='purple' variant='solid'
            disabled={disablingButton()}
            onClick={createOrUpdateFilter}
          >{t('apply-filter')}</Button></ButtonConatiner>
        </MainConatiner> : ''}
    </div>
  </>
  ;
};
export default FilterInput;
const MainConatiner=styled.div`
  display:flex;
  flex-direction:column;
  background: #fff;
  position: absolute;
  top: 50px;
  right: 0;
  padding:14px 20px 10px 20px;
  width: 285px;
  height: 228px;
  box-shadow: 0px 6px 25px rgba(0, 0, 0, 0.15);
  border-radius: 5px;
  z-index: 99;
`
;
const RadioContainer=styled.div`
  display:flex;
  flex-direction:column;
  width: 100%;
  height: 100%;
  .ant-radio:not(.ant-radio-checked) {
    .ant-radio-inner {
      border-color: #d9d9d9 !important;
    }
  }
  input {
    margin-top: 10px;
  }
`;

const ButtonConatiner=styled.div`
display:flex;
justify-content: right;
`;
