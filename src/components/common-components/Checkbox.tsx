import {
  Checkbox as AntdCheckbox,
  CheckboxProps,
} from 'antd';
import styled from 'styled-components';
import {COLORS} from './colors';

export const Checkbox = ({...props}: CheckboxProps) => {
  return (
    <CheckboxStyled {...props}></CheckboxStyled>
  );
};

const CheckboxStyled = styled(AntdCheckbox)`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${COLORS.purple} !important;
    border-color: ${COLORS.purple} !important;
  }
`;

