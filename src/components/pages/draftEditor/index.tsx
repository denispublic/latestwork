import {useCallback, useEffect} from 'react';
// import {ContentEditor} from '@/components/dashboard/pages/landing-page-optimizer/blocks/content-editor';
// import {Notification, notificationType} from '@/components/common-components/components/notification';
import {observer} from 'mobx-react-lite';
// import ShareModal from '../blocks/share-modal';
import {ContentState, EditorState, Modifier, SelectionState} from 'draft-js';
// import {decoratedState} from '../blocks/content-editor/decorators';
import {debounce} from 'lodash';
import { Toolkit } from './toolkit/toolkit';
import {useStore} from '@/store/root-store';
import { EditorSection } from './editorSection';
import styled from 'styled-components';
import { decoratedState } from './decorators';
import { AiTooltip } from './AiTooltip/AiTooltip';

export const DraftEditor = observer(() => {
  const {draftJsStore: {formattedEditorState, setCurrentEditorState, contentFragment, setAiTooltip, aiTooltip}} = useStore('');

  let htmlToDraft = null;
  if (typeof window === 'object') {
    htmlToDraft = require('html-to-draftjs').default;
  }

  const debouncedChange = debounce((state: EditorState, isNewDecorations = false) => {
    // we can use this to debounce call an API to store text on BE
  }, 2000);


  const debouncedUpdate = useCallback(
    (state: EditorState, isNewDecorations) => debouncedChange(state, isNewDecorations),
    // eslint-disable-next-line
        []
  );

  const onChange = (editorState: EditorState, isNewDecorations: boolean = false) => {
    const currentContent = formattedEditorState.getCurrentContent();
    const newContent = editorState.getCurrentContent();

    const decorated = decoratedState(editorState);

    setCurrentEditorState(decorated);

    const lastChangedBlockKey = editorState.getSelection()?.getAnchorKey();
    const lastChangedBlock = currentContent.getBlockForKey(lastChangedBlockKey);
    const lastChangedBlockType = lastChangedBlock?.getType();

    // // We are comparing current editor state with udpated one to make sure the flow only triggers once there is an actual content change (adding removing of words, formatting, linkjng etc.)
    if (currentContent !== newContent || lastChangedBlockType === 'atomic') {
       debouncedUpdate(editorState, isNewDecorations);
    }
  };

  const insertText = async () => {
    // NEW FLOW start
    const {contentBlocks, entityMap} = htmlToDraft(contentFragment);
    console.log('aaaa', contentBlocks, entityMap)

    const selection = formattedEditorState.getSelection();

    const newSelection = new SelectionState({
      anchorKey: selection.getAnchorKey(),
      anchorOffset: selection.getEndOffset(),
      focusKey: selection.getAnchorKey(),
      focusOffset: selection.getEndOffset(),
    });


    const fragment = ContentState.createFromBlockArray(contentBlocks, entityMap).getBlockMap();

    const nonStyledState = Modifier.replaceWithFragment(
      formattedEditorState.getCurrentContent(),
      newSelection,
      fragment,
    );

    // WE are calculating nodes to toggle blue background on new Ai generated nodes
    let firstNewBlockId: number;
    const currentBlock = selection.getAnchorKey();
    const nextBlock = formattedEditorState.getCurrentContent().getBlockAfter(selection.getAnchorKey())?.getKey();

    const existingNodes = document.querySelectorAll('[data-block="true"]');
    existingNodes?.forEach((elem, id) => {
      if (elem.getAttribute('data-offset-key') && elem.getAttribute('data-offset-key') == `${currentBlock}-0-0`) {
        firstNewBlockId = id + 1;
      }
    });


    const updatedState = EditorState.push(formattedEditorState, nonStyledState, 'insert-fragment');
    setCurrentEditorState(updatedState);

    setAiTooltip({show: false});

    setTimeout(() => {
      const newTextElems = document.querySelectorAll('[data-block="true"]');
      if (newTextElems.length) {
        for (let i = firstNewBlockId; i <= newTextElems.length; i++) {
          if (newTextElems[i]?.getAttribute('data-offset-key') && newTextElems[i]?.getAttribute('data-offset-key') != `${nextBlock}-0-0` && newTextElems[i]?.getAttribute('data-offset-key') != `${currentBlock}-0-0`) {
            newTextElems[i].classList.add('newTextBlock');
          } else {
            return;
          }
        }
      }
    }, 500);

    debouncedUpdate(updatedState, false);

    setTimeout(() => {
      const newTextElems = document.querySelectorAll('.newTextBlock');
      if (newTextElems.length) {
        newTextElems.forEach(elem => {
          elem.classList.remove('newTextBlock');
        });
      }
    }, 3500);
    // NEW FLOW END
  };

  const getSelection = useCallback((editorState: EditorState) => {
    return editorState.getSelection();
  }, [formattedEditorState]);

  const getParams = (editorState: EditorState) => {
    const selection = getSelection(editorState);
    const currentContent = editorState.getCurrentContent();
    const startKey = selection.getStartKey();
    const startOffset = selection.getStartOffset();
    const endOffset = selection.getFocusOffset() > selection.getAnchorOffset() ? selection.getFocusOffset() : selection.getAnchorOffset();
    // const startOffset = selection.getAnchorOffset() < selection.getFocusOffset() ? selection.getAnchorOffset() : selection.getFocusOffset();
    const prevOffset = startOffset - 1;
    const blockWithLinkAtBeginning = currentContent.getBlockForKey(startKey);
    const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
    const characterList = blockWithLinkAtBeginning.getCharacterList();
    const initialSelectedText = blockWithLinkAtBeginning.getText().slice(startOffset, endOffset);
    const anchorKey = selection.getAnchorKey();

    



    return {selection, characterList, prevOffset, startOffset, currentContent, blockWithLinkAtBeginning, linkKey, initialSelectedText, anchorKey};
  };

  // WORK IN PROGRESS
  const promptForLink = (editorState: EditorState) => {
    const {selection, characterList, prevOffset, startOffset, currentContent, blockWithLinkAtBeginning, linkKey, initialSelectedText, anchorKey} = getParams(editorState);


    const prevChar = characterList.get(prevOffset);
    const nextChar = characterList.get(startOffset);


    // Get block for current selection
    const currentBlock = currentContent.getBlockForKey(anchorKey);


    let selectedText: string;
    const xSelection = window.getSelection();
    let x;

    let updatedSelection = selection;

    const noEntityCase = () => {
      // this type of check will fix right to left selection where anchor is smaller then focus
      const start = selection.getAnchorOffset() < selection.getFocusOffset() ? selection.getAnchorOffset() : selection.getFocusOffset();
      const end = selection.getFocusOffset() > selection.getAnchorOffset() ? selection.getFocusOffset() : selection.getAnchorOffset();

      selectedText = currentBlock.getText().slice(start, end);
      setAiTooltip({...aiTooltip, selectedText: selectedText.trim(), updatedSelection: selection});
    };

    if (!prevChar || !nextChar) {
      setAiTooltip({...aiTooltip, updatedSelection: updatedSelection});
    } else {
      const prevEntity = prevChar?.getEntity();
      const nextEntity = nextChar?.getEntity();

      const entity = prevEntity === nextEntity && prevEntity || !prevEntity && nextEntity;
      let finalPrevOffset = prevOffset;
      let finalNextOffset = startOffset;

      selectedText = currentBlock.getText().slice(finalPrevOffset, finalNextOffset);

      if (entity) {
        while (finalPrevOffset > 0) {
          finalPrevOffset--;
          const char = characterList.get(finalPrevOffset);
          if (char.getEntity() !== entity) {
            finalPrevOffset++;
            break;
          }
        }


        const blockLength = blockWithLinkAtBeginning.getLength();
        while (finalNextOffset < blockLength) {
          finalNextOffset++;

          const char = characterList.get(finalNextOffset);
          if (char?.getEntity() !== entity) {
            break;
          }
        }
        const finalAnchorOffset = finalPrevOffset === -1 ? 0 : finalPrevOffset;

        updatedSelection = new SelectionState({
          anchorKey: anchorKey, // key of block
          anchorOffset: finalAnchorOffset,
          focusKey: anchorKey,
          focusOffset: finalNextOffset, // key of block
          hasFocus: true,
          // isBackward: false // isBackward = (focusOffset < anchorOffset)
        });


        setAiTooltip({...aiTooltip, updatedSelection: updatedSelection});

        selectedText = currentBlock.getText().slice(finalAnchorOffset, finalNextOffset);

        if (xSelection?.rangeCount) {
          x = xSelection.getRangeAt(0)?.getBoundingClientRect();
          if (x.left) {
            setAiTooltip({...aiTooltip, isLink: true, selectedText: selectedText.trim(), height: x.height, width: x.width, updatedSelection: updatedSelection});
          }
        }
      } else {
        updatedSelection = new SelectionState({
          anchorKey: anchorKey, // key of block
          anchorOffset: finalPrevOffset,
          focusKey: anchorKey,
          focusOffset: finalNextOffset, // key of block
          hasFocus: true,
          // isBackward: false // isBackward = (focusOffset < anchorOffset)
        });

        if (xSelection?.rangeCount) {
          if (initialSelectedText) {
            x = xSelection.getRangeAt(0)?.getBoundingClientRect();
            if (x.left) {
              setAiTooltip({...aiTooltip, isLink: true, selectedText: selectedText.trim(), height: x.height, width: x.width, updatedSelection: updatedSelection});
            }
          }
        }
        noEntityCase();
      }
    }
    let url = '';

    if (linkKey) {
      const linkInstance = currentContent.getEntity(linkKey);
      url = linkInstance.getData().url;
      url = url?.startsWith('//') ? url?.slice(2) : url;
    }

    setAiTooltip({
      ...aiTooltip,
      urlValue: url,
      textValue: selectedText,
    });
  };

useEffect(() => {
  const preventingDefaultMenu = e => {
    e.preventDefault();
    promptForLink(formattedEditorState);
    setAiTooltip({...aiTooltip, show: true, left: e.pageX, top: e.pageY});
  };
  const closeContextMenu = () => {
    setAiTooltip({show: false});
  };
  if (document.querySelectorAll('.DraftEditor-editorContainer')[0]?.addEventListener) {
    document.querySelectorAll('.DraftEditor-editorContainer')[0]?.addEventListener('contextmenu', preventingDefaultMenu, false);
  }
  if (document.querySelectorAll('.DraftEditor-editorContainer')[0]?.addEventListener) {
    document.querySelectorAll('.DraftEditor-editorContainer')[0]?.addEventListener('click', closeContextMenu, false);
  }

  return () => {
    document.querySelectorAll('.DraftEditor-editorContainer')[0]?.removeEventListener('contextmenu', preventingDefaultMenu);
    document.querySelectorAll('.DraftEditor-editorContainer')[0]?.removeEventListener('click', closeContextMenu);
  };
}, []);

    return <EditorWrapper>
      <h1>Draft JS Editor Demo</h1>
      <p>Use toolkit to edit text!</p>
      <p>Checkout link field in toolkit!</p>
      <p>Try hovering over each text block type (yeah, that one on the left), and click on the dropdown!</p>
      <p>Try right clicking on text (you can select some text prior, but you dont have to) and generate some text.</p>
      <p>In next version you will be able to add and edit links from the same dropdown</p>
      <AiTooltip onChange={onChange} insertText={insertText} />
      <Toolkit onChange={onChange} editorState={formattedEditorState} />
      <EditorSection onChange={onChange} editorState={formattedEditorState}/>
    </EditorWrapper>
});

const EditorWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1 0%;
  width: 0%;
  padding: 50px;
`
