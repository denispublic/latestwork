import {Button, Tooltip} from 'antd';

import styles from './style.module.scss';
import {BoldOutlined} from '@ant-design/icons';
import {EditorState} from 'draft-js';
import classNames from 'classnames';

interface Props {
  inlineStyleEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Bold = ({inlineStyleEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Bold'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault(); inlineStyleEnhancement('BOLD');
          }}
          className={classNames(styles.button, {[styles.active]: editorState.getCurrentInlineStyle().toJS().includes('BOLD')} )}
        >
          <BoldOutlined style={{color: editorState.getCurrentInlineStyle().toJS().includes('BOLD') ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
