import {Button, Tooltip} from 'antd';
import styles from './style.module.scss';
import {EditorState, SelectionState} from 'draft-js';
import classNames from 'classnames';
import { LinkOutlined} from '@ant-design/icons';


interface Props {
  editorState: EditorState;
  entityTypeEnhancement: (selection, entityKey) => void;
}

export const AnchorRemove = ({editorState, entityTypeEnhancement}: Props) => {
  const removeLink = (e?: any) => {
    e.preventDefault();
    const selection = editorState.getSelection();
    const currentContent = editorState.getCurrentContent();
    const startKey = selection.getStartKey();
    const startOffset = selection.getStartOffset();
    const blockWithLinkAtBeginning = currentContent.getBlockForKey(startKey);

    const prevOffset = startOffset - 1;
    const characterList = blockWithLinkAtBeginning.getCharacterList();
    const prevChar = characterList.get(prevOffset);
    const nextChar = characterList.get(startOffset);
    const anchorKey = selection.getAnchorKey();


    let updatedSelection;


    if (!prevChar || !nextChar) {
      return;
    } else {
      const prevEntity = prevChar?.getEntity();
      const nextEntity = nextChar?.getEntity();

      const entity = prevEntity === nextEntity && prevEntity;

      if (entity) {
        let finalPrevOffset = prevOffset;
        let finalNextOffset = startOffset;

        while (finalPrevOffset > 0) {
          finalPrevOffset--;
          const char = characterList.get(finalPrevOffset);
          if (char.getEntity() !== entity) {
            finalPrevOffset++;
            break;
          }
        }

        const blockLength = blockWithLinkAtBeginning.getLength();
        while (finalNextOffset < blockLength) {
          finalNextOffset++;

          const char = characterList.get(finalNextOffset);
          if (char?.getEntity() !== entity) {
            break;
          }
        }

        updatedSelection = new SelectionState({
          anchorKey: anchorKey, // key of block
          anchorOffset: finalPrevOffset,
          focusKey: anchorKey,
          focusOffset: finalNextOffset, // key of block
          hasFocus: true,
          // isBackward: false // isBackward = (focusOffset < anchorOffset)
        });

        if (!updatedSelection.isCollapsed()) {
          entityTypeEnhancement(updatedSelection, null);
        }
      } else {
        entityTypeEnhancement(selection, null);
      }
    }
  };

  return (
    <div className={styles['header-toolkit']} style={{position: 'relative'}}>
      <Tooltip title='Remove Link'>
        <Button
          type='text'
          onMouseDown={e => removeLink(e)}
          className={classNames(styles.button, {[styles.active]: editorState.getCurrentInlineStyle().toJS().includes('LINK')} )}
        >
          <LinkOutlined style={{color: 'red'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
