import {Button, Tooltip} from 'antd';

import {EditorState, RichUtils} from 'draft-js';
import classNames from 'classnames';

import styles from './style.module.scss';
import {UnorderedListOutlined} from '@ant-design/icons';

interface Props {
  blockTypeEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const UnorderedList = ({blockTypeEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Unordered list'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault();
            blockTypeEnhancement('unordered-list-item');
          }}
          className={classNames(styles.button, {[styles.active]: RichUtils.getCurrentBlockType(editorState) === 'unordered-list-item'})}
        >
          <UnorderedListOutlined style={{color: RichUtils.getCurrentBlockType(editorState) === 'unordered-list-item' ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
