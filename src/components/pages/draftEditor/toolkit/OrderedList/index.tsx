import {Button, Tooltip} from 'antd';

import {EditorState, RichUtils} from 'draft-js';
import classNames from 'classnames';

import styles from './style.module.scss';
import {OrderedListOutlined} from '@ant-design/icons';

interface Props {
  blockTypeEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const OrderedList = ({blockTypeEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Ordered list'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault();
            blockTypeEnhancement('ordered-list-item');
          }}
          className={classNames(styles.button, {[styles.active]: RichUtils.getCurrentBlockType(editorState) === 'ordered-list-item'})}
        >
          <OrderedListOutlined style={{color: RichUtils.getCurrentBlockType(editorState) === 'ordered-list-item' ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
