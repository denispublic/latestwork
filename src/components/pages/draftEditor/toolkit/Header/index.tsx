import {Select} from 'antd';
import {EditorState, RichUtils} from 'draft-js';
import styles from './style.module.scss';
import {CaretDownOutlined} from '@ant-design/icons';
import {useTranslation} from 'next-i18next';
import classNames from 'classnames';

const {Option} = Select;

interface Props {
  blockTypeEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Header = ({blockTypeEnhancement, editorState}: Props) => {
  const {t} = useTranslation('common');

  const handleSelect = (value: string) => {
    blockTypeEnhancement(value);
  };

  return (
    <div className={classNames(styles['header-toolkit'], styles.blockTypeSelector)}>
      <Select value={RichUtils.getCurrentBlockType(editorState)} style={{width: 'auto'}} onChange={handleSelect} suffixIcon={<CaretDownOutlined style={{color: '#121212', pointerEvents: 'none'}} />}>
        <Option value='header-one'>{t('heading1')}</Option>
        <Option value='header-two'>{t('heading2')}</Option>
        <Option value='header-three'>{t('heading3')}</Option>
        <Option value='header-four'>{t('heading4')}</Option>
        <Option value='header-five'>{t('heading5')}</Option>
        <Option value='header-six'>{t('heading6')}</Option>
        <Option value='unstyled'>{t('paragraph')}</Option>
      </Select>
    </div>
  );
};
