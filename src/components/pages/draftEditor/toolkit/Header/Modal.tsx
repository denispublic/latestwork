import {useStore} from '@/store/root-store';
import {observer} from 'mobx-react-lite';
import {useRef} from 'react';
import styles from './style.module.scss';

const headers = {
  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',
};

interface ModalProps {
  blockTypeEnhancement: (block: string) => void;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Modal: React.FC<ModalProps> = observer(({
  blockTypeEnhancement,
  setOpen,
}) => {
  const el = useRef(null);
  const {contentOptimizer: {currentPage: {content}}} = useStore('');

  const handleSelect = (e: React.MouseEvent<any, MouseEvent>) => {
    e.preventDefault();
    el.current.focus();
    const block = e.currentTarget.getAttribute('data-block');
    blockTypeEnhancement(block);

    if (content && content?.headingLevel === block) {
      content.setHeadingLevel('');
    } else {
      content?.setHeadingLevel(block);
    }
  };

  return (
    <div
      className={styles['header-modal']}
      tabIndex={1}
      ref={el}
      onBlur={() => {
        setOpen(false);
      }}
    >
      {Object.keys(headers).map(val => (
        <span
          key={val}
          data-block={headers[val]}
          onMouseDown={handleSelect}
          className={`${styles['header-modal__option']} ${
            content?.headingLevel === headers[val] ? styles['header-modal__selected'] : ''
          }`}
        >
          {val}
        </span>
      ))}
    </div>
  );
});
