import {useState, useRef, useEffect} from 'react';
import styles from './style.module.scss';
import {LinkOutlined} from '@ant-design/icons';
import {EditorState, Modifier, SelectionState} from 'draft-js';
import classNames from 'classnames';
import {Input, Button} from '@/components/common-components/';
import {useStore} from '@/store/root-store';
import {observer} from 'mobx-react';
import { Tooltip } from 'antd';


interface Props {
  updateText: (state: EditorState) => void;
}

export const Anchor = observer(({updateText}: Props) => {
  const {draftJsStore: {formattedEditorState, setCurrentEditorState}} = useStore('');
  const [showNewUrlEditor, setShowNewUrlEditor] = useState(false);
  const [editorValues, setEditorValues] = useState({
    urlValue: '',
    textValue: '',
  });
  const [selectionRange, setSelectionRange] = useState({customSelection: null});

  const dateRef = useRef(null);

  // Close anchor form popup by clicking anywhere on the screen
  useEffect(() => {
    function handleClickOutside(event) {
      if (dateRef.current && !dateRef.current.contains(event.target)) {
        setShowNewUrlEditor(false);
        setEditorValues({
          urlValue: '',
          textValue: '',
        });
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dateRef]);


  // This will generate text to insert inline on current cursor location
  const insertText = customSelection => {
    const currentContent = formattedEditorState.getCurrentContent();

    const urlPattern = /^((http|https):\/\/)/;

    const formatUrl = urlPattern.test(editorValues.urlValue) ? editorValues.urlValue : `http://${editorValues.urlValue}`;

    const newContentWithEntity = currentContent.createEntity(
      'LINK',
      'MUTABLE',
      {url: formatUrl},
    );

    const entityKey = newContentWithEntity.getLastCreatedEntityKey();

    const newNewContent = Modifier.replaceText(
      currentContent,
      customSelection,
      editorValues.textValue,
      null,
      entityKey,
    );

    const withLinkText = EditorState.createWithContent(
      newNewContent,
    );

    const withProperCursor = EditorState.forceSelection(
      withLinkText,
      withLinkText.getCurrentContent().getSelectionAfter(),
    );


    return withProperCursor;
  };

  // This will add generated text (with link or without) into our text
  const sendTextToEditor = () => {
    const contentState = insertText(selectionRange.customSelection);
    updateText(contentState);
    setShowNewUrlEditor(false);
    setEditorValues({
      urlValue: '',
      textValue: '',
    });
  };


  const promptForLink = () => {
    const selection = formattedEditorState.getSelection();
    const currentContent = formattedEditorState.getCurrentContent();
    const startKey = selection.getStartKey();
    const startOffset = selection.getStartOffset();
    const blockWithLinkAtBeginning = currentContent.getBlockForKey(startKey);
    const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
    const prevOffset = startOffset - 1;

    const characterList = blockWithLinkAtBeginning.getCharacterList();
    const prevChar = characterList.get(prevOffset);
    const nextChar = characterList.get(startOffset);

    // Get block for current selection
    const anchorKey = selection.getAnchorKey();
    const currentBlock = currentContent.getBlockForKey(anchorKey);

    let selectedText;
    let customSelection = selection;

    const noEntityCase = () => {
      const start = selection.getAnchorOffset() < selection.getFocusOffset() ? selection.getAnchorOffset() : selection.getFocusOffset();
      const end = selection.getFocusOffset() > selection.getAnchorOffset() ? selection.getFocusOffset() : selection.getAnchorOffset();

      setSelectionRange({customSelection});
      selectedText = currentBlock.getText().slice(start, end);
    };

    if (!prevChar || !nextChar) {
      noEntityCase();
    } else {
      const prevEntity = prevChar?.getEntity();
      const nextEntity = nextChar?.getEntity();

      const entity = prevEntity === nextEntity && prevEntity;

      if (entity) {
        let finalPrevOffset = prevOffset;
        let finalNextOffset = startOffset;

        while (finalPrevOffset > 0) {
          finalPrevOffset--;
          const char = characterList.get(finalPrevOffset);
          if (char.getEntity() !== entity) {
            finalPrevOffset++;
            break;
          }
        }

        const blockLength = blockWithLinkAtBeginning.getLength();
        while (finalNextOffset < blockLength) {
          finalNextOffset++;

          const char = characterList.get(finalNextOffset);
          if (char?.getEntity() !== entity) {
            break;
          }
        }

        selectedText = currentBlock.getText().slice(finalPrevOffset, finalNextOffset);

        customSelection = new SelectionState({
          anchorKey: anchorKey, // key of block
          anchorOffset: finalPrevOffset,
          focusKey: anchorKey,
          focusOffset: finalNextOffset, // key of block
          hasFocus: true,
          // isBackward: false // isBackward = (focusOffset < anchorOffset)
        });


        setSelectionRange({customSelection: customSelection});
      } else {
        noEntityCase();
      }
    }


    let url = '';

    if (linkKey) {
      const linkInstance = currentContent.getEntity(linkKey);
      url = linkInstance.getData().url;
      url = url.startsWith('//') ? url.slice(2) : url;
    }


    setShowNewUrlEditor(true);
    setEditorValues({
      urlValue: url,
      textValue: selectedText,
    });
  };


  // This is called when we click anchor icon in toolbar
  const iconClicked = (e?: any) => {
    e.preventDefault();
    promptForLink();
  };

  return (
    <div className={styles['header-toolkit']} style={{position: 'relative'}}>
      <Tooltip title='Link'>
        <div ref={dateRef}>
        <Button
          type='text'
          variant='ghost'
          color='white'
          onMouseDown={e => iconClicked(e)}
          className={classNames(styles.button, {[styles.active]: formattedEditorState.getCurrentInlineStyle().toJS().includes('LINK')} )}
        >
          <LinkOutlined style={{color: formattedEditorState.getCurrentInlineStyle().toJS().includes('LINK') ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
        </div>
      
      </Tooltip>
      {showNewUrlEditor && <div className={styles.urlPopup}>
        <div className={styles.inputContainer}>
          <p className={styles.textLabel}>Text</p>
          <Input
            onMouseDown={e => e.stopPropagation()}
            onChange={e => setEditorValues({
              ...editorValues,
              textValue: e.target.value,
            })}
            type='text'
            value={editorValues.textValue}
            placeholder='Add text'
            className={styles.urlInput}
          />
          <Input
            onMouseDown={e => e.stopPropagation()}
            onChange={e => setEditorValues({
              ...editorValues,
              urlValue: e.target.value,
            })}
            type='text'
            value={editorValues.urlValue}
            style={{marginBottom: '0px'}}
            className={styles.urlInput}
            placeholder='Paste a link'
          />
        </div>
        <div style={{display: 'flex'}}>
          <Tooltip title='Add link'>
            <Button onMouseDown={sendTextToEditor} className={styles.inputControlls} disabled={!editorValues.textValue || !editorValues.urlValue}>
              Apply
            </Button>
          </Tooltip>
        </div>
      </div>}
    </div>
  );
});
