import {Button as CommonButton} from '@/components/common-components/components';
import {faImage} from '@fortawesome/pro-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Tooltip} from 'antd';
import styles from '../../style.module.scss';


interface Props {
  openDrawer: (value: boolean) => void;
}

export const AddImage = ({openDrawer}: Props) => {
  return <Tooltip title='Search and insert images into the article.'>
    <CommonButton
      style={{display: 'flex', marginRight: 10}}
      buttonType='transparent'
      onClick={() => openDrawer(true)}
      className={styles.share}
    >
      <FontAwesomeIcon icon={faImage} style={{marginRight: '5px', height: '14px'}} className={styles.robotIcon} />
      <span className={styles.shareBtnLabel}>
      Add Image
      </span>
    </CommonButton>
  </Tooltip>;
};
