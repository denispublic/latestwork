import {Button, Tooltip} from 'antd';

import styles from './style.module.scss';
import {ItalicOutlined} from '@ant-design/icons';
import {EditorState} from 'draft-js';
import classNames from 'classnames';

interface Props {
  inlineStyleEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Italic = ({inlineStyleEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Italic'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault(); inlineStyleEnhancement('ITALIC');
          }}
          className={classNames(styles.button, {[styles.active]: editorState.getCurrentInlineStyle().toJS().includes('ITALIC')} )}
        >
          <ItalicOutlined style={{color: editorState.getCurrentInlineStyle().toJS().includes('ITALIC') ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
