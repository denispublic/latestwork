import {Button, Tooltip} from 'antd';

import styles from './style.module.scss';
import {UnderlineOutlined} from '@ant-design/icons';
import {EditorState} from 'draft-js';
import classNames from 'classnames';

interface Props {
  inlineStyleEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Underline = ({inlineStyleEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Underline'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault(); inlineStyleEnhancement('UNDERLINE');
          }}
          className={classNames(styles.button, {[styles.active]: editorState.getCurrentInlineStyle().toJS().includes('UNDERLINE')} )}
        >
          <UnderlineOutlined style={{color: editorState.getCurrentInlineStyle().toJS().includes('UNDERLINE') ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
