import isURL from 'validator/lib/isURL';
import isEmpty from 'lodash/isEmpty';
import {useRef, useState} from 'react';
import {Input, notification} from 'antd';
import {EditorState, Modifier} from 'draft-js';
import {Button} from 'antd';
import {IFocusTermToClassNameMap} from '@/store/content-optimizer/currentPage';

import styles from './style.module.scss';

interface ModalProps {
  editorState: EditorState;
  setEditorState: React.Dispatch<React.SetStateAction<EditorState>>;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  focusTermClassNames: IFocusTermToClassNameMap;
}

export const Modal: React.FC<ModalProps> = ({
  setOpen,
  editorState,
  setEditorState,
}) => {
  const el = useRef(null);
  const [link, setLink] = useState('');

  const handleAddLink = (url: string) => {
    if (!isURL(url, {require_protocol: true}) || isEmpty(editorState)) {
      notification.error({
        message: 'Invalid link',
        description: 'The link you tried to attach is invalid.',
        placement: 'bottomRight',
      });
      return;
    }

    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'LINK',
      'MUTABLE',
      {url},
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const contentStateWithLink = Modifier.applyEntity(
      contentStateWithEntity,
      editorState.getSelection(),
      entityKey,
    );
    const newEditorState = EditorState.push(
      editorState,
      contentStateWithLink,
      'apply-entity',
    );
    setEditorState(newEditorState);
  };

  return (
    <div
      className={styles['header-modal']}
      tabIndex={1}
      ref={el}
      onBlur={() => {
        setOpen(false);
      }}
    >
      <Input
        placeholder='Url'
        value={link}
        onChange={({target: {value}}) => setLink(value)}
      />
      <Button onMouseDown={() => handleAddLink(link)}>Attach</Button>
    </div>
  );
};
