import {useState} from 'react';
import {Button} from 'antd';
import {IconAttach} from '../../Icons';

import styles from './style.module.scss';
import {Modal} from './Modal';
import {EditorState} from 'draft-js';
import {IFocusTermToClassNameMap} from '@/store/content-optimizer/currentPage';

interface HyperLinkProps {
  setEditorState: React.Dispatch<React.SetStateAction<EditorState>>;
  editorState: EditorState;
  focusTermClassNames: IFocusTermToClassNameMap;
}

export const HyperLink: React.FC<HyperLinkProps> = ({
  editorState,
  setEditorState,
  focusTermClassNames,
}) => {
  const [open, setOpen] = useState(false);

  return (
    <div className={styles['header-toolkit']}>
      <Button
        type='text'
        data-block='header-three'
        onMouseDown={() => setOpen(prev => !prev)}
      >
        <IconAttach />
      </Button>
      {open && (
        <Modal
          setOpen={setOpen}
          editorState={editorState}
          setEditorState={setEditorState}
          focusTermClassNames={focusTermClassNames}
        />
      )}
    </div>
  );
};
