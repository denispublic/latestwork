import {EditorState, RichUtils} from 'draft-js';
import styles from '../style.module.scss';
import {Button, Tooltip} from 'antd';
import {Header} from './Header';
import {UnorderedList} from './UnorderedList';
import {RedoOutlined, UndoOutlined} from '@ant-design/icons';
import {observer} from 'mobx-react-lite';
import {Bold} from './Bold';
import {Italic} from './Italic';
import {Underline} from './Underline';
import {OrderedList} from './OrderedList';
import {Code} from './Code';
import {Anchor} from './Anchor';
import classNames from 'classnames';
import {AnchorRemove} from './AnchorRemove';
import {isNil} from 'lodash';

interface Props {
  editorState: EditorState;
  onChange: (state: EditorState, isNewDecorations: boolean) => void;
}

export const Toolkit = observer(({editorState, onChange}: Props) => {
  const blockTypeEnhancement = (block: string) => {
    onChange(RichUtils.toggleBlockType(editorState, block), true);
  };

  const inlineStyleEnhancement = (style: string) => {
    onChange(RichUtils.toggleInlineStyle(editorState, style), true);
  };

  const entityTypeEnhancement = (selection: any, entityKey: string) => {
    onChange(RichUtils.toggleLink(editorState, selection, entityKey), true);
  };

  const updateText = (newState: EditorState) => {
    onChange(newState, true);
  };

  const undo = () => {
    onChange(EditorState.undo(editorState), true);
  };

  const redo = () => {
    onChange(EditorState.redo(editorState), true);
  };

  return (
    <div className={styles.toolbar} style={{display:  'flex'}}>
      <div className={styles.leftSection}>
        <Header editorState={!isNil(editorState) ? editorState : EditorState.createEmpty()} blockTypeEnhancement={blockTypeEnhancement} />
        <Bold editorState={editorState} inlineStyleEnhancement={inlineStyleEnhancement} />
        <Italic editorState={editorState} inlineStyleEnhancement={inlineStyleEnhancement} />
        <Underline editorState={editorState} inlineStyleEnhancement={inlineStyleEnhancement} />
        <Anchor updateText={updateText} />
        <AnchorRemove editorState={editorState} entityTypeEnhancement={entityTypeEnhancement} />
        {/* <Highlight editorState={editorState} inlineStyleEnhancement={inlineStyleEnhancement} /> */}
        <OrderedList editorState={editorState} blockTypeEnhancement={blockTypeEnhancement} />
        <UnorderedList editorState={editorState} blockTypeEnhancement={blockTypeEnhancement} />
        {/* <Monospace editorState={editorState} inlineStyleEnhancement={inlineStyleEnhancement} /> */}
        <Code editorState={editorState} blockTypeEnhancement={blockTypeEnhancement} />
        <div className={styles.divider}></div>
        <Tooltip title='Undo'>
          <Button type='text' className={classNames(styles.button, styles.undoBtn)} onClick={undo}><UndoOutlined style={{color: '#4E5156'}} /></Button>
        </Tooltip>
        <Tooltip title='Redo'>
          <Button type='text' className={classNames(styles.button, styles.redoBtn)} onClick={redo}><RedoOutlined style={{color: '#4E5156'}} /></Button>
        </Tooltip>
        {/* <Tooltip title='Zooming feature does not change the text size'>
          <SelectStyled value={zoomLevel} onChange={value => setZoomLevel(value)} suffixIcon={<CaretDownOutlined style={{color: '#121212', pointerEvents: 'none'}} />}>
            <Select.Option value={0.5}>50%</Select.Option>
            <Select.Option value={0.75}>75%</Select.Option>
            <Select.Option value={0.9}>90%</Select.Option>
            <Select.Option value={1}>100%</Select.Option>
            <Select.Option value={1.2}>120%</Select.Option>
            <Select.Option value={1.5}>150%</Select.Option>
            <Select.Option value={2}>200%</Select.Option>
          </SelectStyled>
        </Tooltip> */}
      </div>
    </div>

  );
});
