import {Button, Tooltip} from 'antd';

import {EditorState, RichUtils} from 'draft-js';
import classNames from 'classnames';

import styles from './style.module.scss';
import {CodeOutlined} from '@ant-design/icons';

interface Props {
  blockTypeEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Code = ({blockTypeEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Tooltip title='Code block'>
        <Button
          type='text'
          onMouseDown={e => {
            e.preventDefault();
            blockTypeEnhancement('code-block');
          }}
          className={classNames(styles.button, {[styles.active]: RichUtils.getCurrentBlockType(editorState) === 'code-block'})}
        >
          <CodeOutlined style={{color: RichUtils.getCurrentBlockType(editorState) === 'code-block' ? '#2D6CCA' : '#4E5156'}}/>
        </Button>
      </Tooltip>
    </div>
  );
};
