import {Button} from 'antd';

import styles from './style.module.scss';
import {ColumnWidthOutlined} from '@ant-design/icons';
import {EditorState} from 'draft-js';
import classNames from 'classnames';


interface Props {
  inlineStyleEnhancement: (e: any) => void;
  editorState: EditorState;
}

export const Monospace = ({inlineStyleEnhancement, editorState}: Props) => {
  return (
    <div className={styles['header-toolkit']}>
      <Button
        type='text'
        onMouseDown={e => {
          e.preventDefault(); inlineStyleEnhancement('MONOSPACE');
        }}
        className={classNames(styles.button, {[styles.active]: editorState.getCurrentInlineStyle().toJS().includes('MONOSPACE')} )}
      >
        <ColumnWidthOutlined style={{color: editorState.getCurrentInlineStyle().toJS().includes('MONOSPACE') ? '#2D6CCA' : '#4E5156'}}/>
      </Button>
    </div>
  );
};
