import {LoadingOutlined} from '@ant-design/icons';
import {useStore} from '@/store/root-store';
import styles from './styles.module.scss';
import {Tooltip} from 'antd';
import {EditorState, Modifier, RichUtils} from 'draft-js';
import {Input, Button} from '@/components/common-components/';
import {observer} from 'mobx-react';
import classNames from 'classnames';
import { createCompositeDecorators } from '../decorators';
import {EditOutlined, LinkOutlined, CopyOutlined} from '@ant-design/icons';


interface Props {
  onChange: (editorState: EditorState, isNewDecorations: boolean) => void;
  insertText: () => void;
}

export const AiTooltip = observer(({onChange, insertText}: Props) => {
  const {draftJsStore: {aiTooltip, setAiTooltip, contentFragment, isCopy, setIsCopy, formattedEditorState}} = useStore('');
  const antIcon = <LoadingOutlined style={{fontSize: 16, color: '#7f4ead', marginLeft: '10px'}} spin />;

  const fetchAndInsertText = async (action: 'insert' | 'copy') => {
      if (action === 'insert') {
        insertText();
      }

      if (action === 'copy') {
        setAiTooltip({...aiTooltip, show: false});
        if (contentFragment) {
          navigator.clipboard.writeText(contentFragment);
          setIsCopy(true);

          setTimeout(() => {
            setIsCopy(false);
          }, 5000);
        }
      }
  };

  const onLinkRemove = () => {
    if (!aiTooltip?.updatedSelection?.isCollapsed()) {
      onChange(RichUtils.toggleLink(formattedEditorState, aiTooltip.updatedSelection, null), true);
      setAiTooltip({show: false});
    }
  };

  const sendTextToEditor = () => {
    const contentState = insertLinkToText();
    onChange(contentState, true);
  };

  const insertLinkToText = () => {
    const decorator = createCompositeDecorators();
    const currentContent = formattedEditorState.getCurrentContent();

    const urlPattern = /^((http|https):\/\/)/;

    const formatUrl = urlPattern.test(aiTooltip.urlValue) ? aiTooltip.urlValue : `http://${aiTooltip.urlValue}`;

    const newContentWithEntity = currentContent.createEntity(
      'LINK',
      'MUTABLE',
      {url: formatUrl},
    );

    const entityKey = newContentWithEntity.getLastCreatedEntityKey();

    const selection = aiTooltip.updatedSelection;

    const newNewContent = Modifier.replaceText(
      currentContent,
      selection,
      aiTooltip.textValue,
      null,
      entityKey,
    );

    const withLinkText = EditorState.createWithContent(
      newNewContent,
      decorator,
    );

    const withProperCursor = EditorState.forceSelection(
      withLinkText,
      withLinkText.getCurrentContent().getSelectionAfter(),
    );

    setAiTooltip({...aiTooltip, isLinkEdit: false});
    return withProperCursor;
  };


  return <div
    className={classNames(styles.selectionTooltip)}
    style={{
      display: aiTooltip.show ? 'block' : 'none',
      top: (aiTooltip.top + aiTooltip.height + 15),
      left: aiTooltip.left + ((aiTooltip.width - 300) / 2)}}
  >
    {!aiTooltip.isLinkEdit ? <div className={styles.selectedTextInfoContainer}>
      <div className={styles.dataAndIcon}>
        <div className={styles.textAndUrl}>
          <p className={styles.text}>{aiTooltip.textValue}</p>
          {aiTooltip.urlValue && <p className={styles.url}>{aiTooltip.urlValue}</p>}
        </div>
      </div>
      <div className={styles.textUrlControlls}>
        <CopyOutlined style={{width: '15px', marginRight: 10}} onClick={() => {
          if (!aiTooltip.selectedText) return;
          navigator.clipboard.writeText(aiTooltip.selectedText);
        }}/>
        <EditOutlined  onClick={e => {
          e.stopPropagation();
          setAiTooltip({...aiTooltip, isLinkEdit: true});
        }}/>
        {aiTooltip.isLink &&  <LinkOutlined style={{color: 'red'}} onClick={() => onLinkRemove()}/> }
      </div>
    </div> :
      <div className={styles.urlPopup}>
        <div className={styles.inputContainer}>
          <p className={styles.textLabel}>Text</p>
          <Input
            onMouseDown={e => e.stopPropagation()}
            onChange={e => setAiTooltip({
              ...aiTooltip,
              textValue: e.target.value,
            })}
            type='text'
            value={aiTooltip.textValue}
            placeholder='Add text'
            className={styles.urlInput}
          />
          <Input
            onMouseDown={e => e.stopPropagation()}
            onChange={e => setAiTooltip({
              ...aiTooltip,
              urlValue: e.target.value,
            })}
            type='text'
            value={aiTooltip.urlValue}
            style={{marginBottom: '0px'}}
            className={styles.urlInput}
            placeholder='Paste a link'
          />
        </div>
        <div style={{display: 'flex', flexDirection: 'column', paddingTop: 20}}>
          <Button variant='ghost' color='textGrayLight' onMouseDown={() => setAiTooltip({...aiTooltip, isLinkEdit: false})} className={styles.inputControlls} style={{paddingLeft: 14}}>
        Cancel
          </Button>
          <Tooltip title='Add link'>
            <Button onMouseDown={sendTextToEditor} className={styles.inputControlls} disabled={!aiTooltip.textValue || !aiTooltip.urlValue}>
          Apply
            </Button>
          </Tooltip>
        </div>
      </div>}
    <Button
      variant='ghost'
      style={{color: '#121212', fontSize: 14, width: 280, marginBottom: 10}}
      onClick={e => {
        e.stopPropagation();
        fetchAndInsertText('insert');
      }}>
      <span>Write a Paragraph for me</span>
    </Button>
    <Button
      variant='ghost'
      style={{color: '#121212', fontSize: 14, width: 280}}
      onClick={e => {
        e.stopPropagation();
        fetchAndInsertText('copy');
      }}>
      <span style={{fontSize: 14}}>Write Paragraph &amp; Copy to Clipboard</span>
    </Button>
  </div>;
});

