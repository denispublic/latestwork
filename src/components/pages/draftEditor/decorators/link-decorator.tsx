import {Popover} from 'antd';
import {ContentBlock, ContentState, DraftDecorator} from 'draft-js';
import {StrategyCallbackType} from './types';

type LinkComponentProps = {
  contentState: ContentState;
  entityKey: string;
  children: React.ReactNode;
  decoratedText: any;
};

export const ExternalLinkPopupContent = ({url}: {url: string}) => {
  return (
    <a href={url} target='_blank' rel='noopener noreferrer'>
      {/* this check is added because in Anchor tool we add // to links that dont have protocols */}
      Open <span style={{textDecoration: 'underline'}}>{url.startsWith('http://') ? url.slice(7) : url}</span>
    </a>
  );
};

export const LinkComponent = (props: LinkComponentProps) => {
  const {url} = props.contentState.getEntity(props.entityKey).getData();
  const searchKey = props.decoratedText.toLocaleLowerCase();

  return (
    <Popover content={<ExternalLinkPopupContent url={url} />}>
      <a href={url}
        style={{textDecoration: 'underline', color: 'blue'}}>{props.children}</a>
    </Popover>
  );
};

function linkStrategy(
  contentBlock: ContentBlock,
  callback: StrategyCallbackType,
  contentState: ContentState,
) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
}

export const createLinkDecorator = (): DraftDecorator => {
  return {
    strategy: linkStrategy,
    component: LinkComponent,
  };
};
