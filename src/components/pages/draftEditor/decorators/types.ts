export type StrategyCallbackType = (start: number, end: number) => void;
