import {CompositeDecorator, EditorState} from 'draft-js';
import {createLinkDecorator} from './link-decorator';

export const createCompositeDecorators = () =>
  new CompositeDecorator([createLinkDecorator()]);

export const decoratedState = (state: EditorState): EditorState => {
  return EditorState.set(state, {decorator: createCompositeDecorators()});
};
