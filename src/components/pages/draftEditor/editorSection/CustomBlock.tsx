import {EditorBlock, RichUtils} from 'draft-js';
import {Select} from 'antd';
import styled from 'styled-components';
import {useEffect, useRef, useState} from 'react';
import {CaretDownOutlined} from '@ant-design/icons';



export const CustomBlock = props => {
  const {Option} = Select;
  const [isOpen, setIsOpen] = useState(false);


  const typesMap = {
    'header-one': 'H1',
    'header-two': 'H2',
    'header-three': 'H3',
    'header-four': 'H4',
    'header-five': 'H5',
    'header-six': 'H6',
    'unstyled': 'p',
  };

  const writeForMeTopOffset = {
    'header-one': '12px',
    'header-two': '5px',
    'header-three': '-2px',
    'header-four': '-2px',
    'header-five': '-2px',
    'header-six': '-2px',
    'unstyled': '-3px',
  };

  const {block, blockProps} = props;
  const blockType = block.getType();

  const blockRef = useRef(null);

  useEffect(() => {
    const preventingDefaultMenu = e => {
      e.stopPropagation();
    };

    document.querySelectorAll('#CustomDropdown')?.forEach(elem => {
      if (elem.addEventListener) elem.addEventListener('contextmenu', preventingDefaultMenu, false);
    });


    return () => {
      document.querySelectorAll('#CustomDropdown')?.forEach(elem => {
        elem.removeEventListener('contextmenu', preventingDefaultMenu, false);
      });
    };
  }, []);

  const dropdownRef = useRef<HTMLInputElement>();
  useEffect(() => {
    function handleClickOutside(event) {
      if (dropdownRef.current && (!dropdownRef.current.contains(event.target) )) {
        setTimeout(() => setIsOpen(false), 300);
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dropdownRef]);


  // we are adding small timeout because by default click on this elem will trigger DraftJS rerender, and that will close the popup
  const handleOpenMenu = () => {
    setTimeout(() => setIsOpen(!isOpen), 300);
  };

  const handleChange = value => {
    blockProps.onChange(RichUtils.toggleBlockType(blockProps.editorState, value), true);
  };


  return <BlockContainer ref={blockRef} blockHeight={blockRef?.current?.offsetHeight} topOffset={writeForMeTopOffset[blockType]}>
    <DropdownContainer
      blockHeight={blockRef?.current?.offsetHeight}
      id='CustomDropdown' onContextMenu={() => {
        return false;
      }}
      currentValue={typesMap[blockType]}
      isOpen={isOpen}
      ref={dropdownRef}>
      <Select
        defaultValue={typesMap[blockType]}
        open={isOpen}
        onChange={handleChange}
        onClick={handleOpenMenu}
        suffixIcon={<CaretDownOutlined />}>
        {Object.keys(typesMap).map((type, idx) => {
          return <Option key={idx} value={type}>{typesMap[type]}</Option>;
        })}
      </Select>
    </DropdownContainer>
    <EditorBlock {...props} />
  </BlockContainer>
};

const BlockContainer = styled.div<{blockHeight?: number; topOffset?: string}>`
  position: relative;
  .writeForMeContainer {
    width: 127px;
    position: absolute;
    left: -182px;
    top: ${p => p.topOffset};
    display: none;
    z-index: 9999999;
    height: ${p => `${p.blockHeight}px` || 'auto'};
  }

  &::before {
    content: '';
    width: 150px;
    height: 100%;
    position: absolute;
    left: -150px;
    cursor: default !important;
  }
`;

// We are passing down currentValue and showing it as an before element to prevent it to be selected when selecting other text in editor
const DropdownContainer = styled.div<{currentValue?: string; isOpen?: boolean; blockHeight?: number}>`
  outline: none !important;
  position: absolute;
  left: -55px !important;
  top: -3px !important;
  z-index: 999999;
  height: ${p => `${p.blockHeight}px` || 'auto'};

  .ant-select {
    width: 46px !important;
    z-index: 999999;


    .ant-select-selection-item {
      display: none;
      padding-right: 0px !important;
      color: #A3A4A4 !important;

    }

    &.ant-select-open {
      .ant-select-selection-item {
        padding-right: 0px !important;
        color: #000 !important;
      }
    }
  }

  .ant-select-selector {
    padding: 0 5px !important;
    border-radius: 4px !important;   

    &::before {
      content: '${p => p.currentValue}';
      width: 100%;
      height: 100%;
      pointer-events: none;
      user-select: none;
      color: #A3A4A4 !important;
      margin: ${p => p.currentValue == 'p' ? '1px 2px 4px 4px' : '3px 2px 4px auto'};
    }

    border: ${p => p.isOpen ? ' border: 1px solid #E8E8E8 !important' : '1px solid transparent !important;'} 
  }
  .ant-select-arrow {
    right: 8px !important;
    width: 10px !important;
    height: 10px !important;
    display: none;
  }

  &:hover, &:active, &:focus {
    outline: none !important;

    .ant-select-selector {
      border: 1px solid #E8E8E8 !important;
    }

    .ant-select-arrow {
      display: block;
    }
  }


`;
