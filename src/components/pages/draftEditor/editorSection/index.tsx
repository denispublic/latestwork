import {useRef} from 'react';
// import {ContentEditor} from '@/components/dashboard/pages/landing-page-optimizer/blocks/content-editor';
// import {Notification, notificationType} from '@/components/common-components/components/notification';
import {observer} from 'mobx-react-lite';
// import ShareModal from '../blocks/share-modal';
import {EditorState, RichUtils, KeyBindingUtil, getDefaultKeyBinding, Modifier, ContentState, DraftHandleValue} from 'draft-js';
// import {decoratedState} from '../blocks/content-editor/decorators';
import Editor, {composeDecorators} from '@draft-js-plugins/editor';
import createImagePlugin from '@draft-js-plugins/image';
import createFocusPlugin from '@draft-js-plugins/focus';
import createResizeablePlugin from '@draft-js-plugins/resizeable';
import { CustomBlock } from './CustomBlock';


const focusPlugin = createFocusPlugin();
const resizeablePlugin = createResizeablePlugin();

const decorator = composeDecorators(
  resizeablePlugin.decorator,
  focusPlugin.decorator,
);
const imagePlugin = createImagePlugin({decorator});

const editorPlugins = [imagePlugin, focusPlugin, resizeablePlugin];

interface Props {
  editorState: EditorState;
  onChange: (state: EditorState, isNewDecorations?: boolean) => void;
}


export const EditorSection = observer(({editorState, onChange}: Props) => {

  
  let htmlToDraft = null;
  if (typeof window === 'object') {
    htmlToDraft = require('html-to-draftjs').default;
  }

const editor = useRef(null);

// we use custom blocks so we can manipulate them further, like add custom block controls etc.
const myBlockRenderer = contentBlock => {
  const blockTypes = ['header-one', 'header-two', 'header-three', 'header-four', 'header-five', 'header-six', 'unstyled'];
  const type = contentBlock.getType();
  if (blockTypes.includes(type)) {
    return {
      component: CustomBlock,
      editable: true,
      props: {
        onChange: onChange,
        editorState: editorState,
      },
    };
  }
};

// This is used to generate css classes that will define styling of each block
const customBlockStyleFn = contentBlock => {
    const type = contentBlock.getType();
    if (type === 'code-block') {
      return 'codeBlock';
    } else if (type === 'header-one') {
      return 'headerOneWithTag';
    } else if (type === 'header-two') {
      return 'headerTwoWithTag';
    } else if (type === 'header-three') {
      return 'headerThreeWithTag';
    } else if (type === 'header-four') {
      return 'headerFourWithTag';
    } else if (type === 'header-five') {
      return 'headerFiveWithTag';
    } else if (type === 'header-six') {
      return 'headerSixWithTag';
    } else if (type === 'unstyled') {
      return 'paragraphWithTag';
    }
  };

  const handleKeyCommand = (command, editorState) => {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return 'handled';
    }

    return 'not-handled';
  };

  const keyBindingFn = event => {
    // we press CTRL + K => return 'bbbold'
    // we use hasCommandModifier instead of checking for CTRL keyCode because different OSs have different command keys
    if (KeyBindingUtil.hasCommandModifier(event) && event.keyCode === 75) {
      return 'new-link';
    }
    // manages usual things, like:
    // Ctrl+Z => return 'undo'
    return getDefaultKeyBinding(event);
  };

  
  const handlePastedText = (text: string, html: string, editorState: EditorState): DraftHandleValue => {
    if (!html) {
      const {contentBlocks, entityMap} = htmlToDraft(text);

      const selection = editorState.getSelection();

      const fragment = ContentState.createFromBlockArray(contentBlocks, entityMap).getBlockMap();

      const contentState = Modifier.replaceWithFragment(
        editorState.getCurrentContent(),
        selection,
        fragment,
      );

      const updatedState = EditorState.push(editorState, contentState, 'insert-fragment');

      const withProperCursor = EditorState.forceSelection(
        updatedState,
        contentState.getSelectionAfter(),
      );

      onChange(withProperCursor);
      return 'handled';
    } else {
      return 'not-handled';
    }
  };

  return <>
   <Editor
        editorKey='editor'
        ref={editor}
        onChange={draftEditorState => {
            onChange(draftEditorState);
        }}
        editorState={editorState as EditorState}
        handlePastedText={handlePastedText}
        placeholder={'Start typing here...'}
        blockStyleFn={customBlockStyleFn}
        handleKeyCommand={handleKeyCommand}
        keyBindingFn={keyBindingFn}
        blockRendererFn={myBlockRenderer}
        plugins={editorPlugins}
    />
  </>;
});