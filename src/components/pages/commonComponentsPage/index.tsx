import {Accordion, AccordionPanel} from "@/components/common-components/Accordion";
import {Button, Input } from "@/components/common-components";
import { Alert, Divider, Tooltip } from "antd";
import { useState } from "react";
import styles from './styles.module.scss';
import {keywordListValidation, validationErrorMessage} from '@/utils/validations/keyword';
import { CustomTextarea } from "@/components/common-components/CustomTextArea/CustomTextrea";
import {stringToArray} from '@/utils/arrays';
import {keywordNewLineReg} from '@/constants/regex';
import {WarningOutlined, TransactionOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { CountryDropdown } from "@/components/common-components/CountryDropdown";
import { FloatingActionButton } from "@/components/common-components/FloatingActionButton";
import { Skeleton } from "@/components/common-components/Skeleton";


export const CommonComponentsPage = () => {
    const [showSkeletonLoader, setShowSkeletonLoader] = useState(false);
    const [isEmptyInput, setIsEmptyInput] = useState(false);
    const [findKeyword, setFindKeyword] = useState<boolean>(false);
    const [keywords, setKeywords] = useState<string>('');
    const [finalKwList, setFinalKwList]  = useState<string[]>([]);
    const [errorKeywordList, setErrorKeywordList] = useState<string[]>([]);
    const [showKwErrorMsg, setShowKwErrorMsg] = useState({
        showMessage: false,
        message: '',
      });


    const [expandedAccordion, setExpandedAccordion] = useState(false);
    const [activeAccordion, setActiveAccordion] = useState<string | string[]>('0');

    const formatKeywordError = errors => {
        const emptyKeyword = 'Empty lines are not allowed';
        return errors.map((item, index) =>{
          const text = item ? item : emptyKeyword;
          return (<li key={index}>
            {text}
          </li>);
        });
      };

    const handleKeywords = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        if (keywords) setIsEmptyInput(false);
        const newValue = e.target.value.trim();
        setKeywords(newValue);
        const validationResult = keywordListValidation(newValue);
        setFindKeyword(validationResult.isValid);
        setErrorKeywordList(validationResult.errorKeywordList);
    };

    const onSubmitKeywords = () => {
        const updatedKeywordList = stringToArray(keywords, keywordNewLineReg, ',' );
    
        const format = /[`~!@#$%^*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        let isKwError = false;
    
        updatedKeywordList.forEach(kw => {
          if (format.test(kw)) {
            setShowKwErrorMsg({
              showMessage: true,
              message: 'Only characters and numbers are allowed',
            });
            isKwError = true;
            return;
          }
        });
    
        if (isKwError) return;
    
        setShowKwErrorMsg({
          showMessage: false,
          message: '',
        });

        setFinalKwList(updatedKeywordList)
      };

      const accordionItems = ['item1', 'item2', 'item3']
    return <CommonComponentsWrapper>
        {/* CUSTOM TEXTAREA */}
        <div>
            <h2>Custom text area for adding multiple keywords</h2>
            <div style={{display: 'flex'}}>
                <div>
                    <CustomTextarea autoSize={true} rows={2} minHeight = '38px' width ='354px' variant='dark' autoSizeCheck={true} placeholder={'Enter a keyword like "Garden Tools"'} onChange={handleKeywords} ></CustomTextarea>
                    <div className={styles.kwInputDesc}>Separate multiple keywords by comma <span className={styles.kwDescriptionBtnWithBg}>,</span> or <span className={styles.kwDescriptionBtnWithBg}>shift</span> + <span className={styles.kwDescriptionBtnWithBg}>enter</span></div>
                    {isEmptyInput && <p style={{color: '#ff4d4f', marginBottom: '0', textAlign: 'center', position: 'absolute', left: '40%', bottom: '-18px'}}>Input a valid query</p>}
                    {!findKeyword && keywords != '' ? <Alert className={styles.errorMessage} message={`${validationErrorMessage}`} description={ <ul>{formatKeywordError(errorKeywordList)} </ul>} type='error' /> : ''}
                </div>
                {showKwErrorMsg.showMessage && <Tooltip title={showKwErrorMsg.message}><WarningOutlined color='red' style={{marginTop: 15, fontSize: 16, marginBottom: 'auto', marginLeft: 10}}/></Tooltip> }
                <div>
                    <ul>    
                        {finalKwList.map((kw, idx) => {
                            return <li key={idx}>{kw}</li>
                        })}
                    </ul>
                </div>
            </div>
            <Button
                variant='ghost'
                onClick={onSubmitKeywords}
                color='purple'
            >
                Submit Keywords
            </Button>
        </div>

        <Divider />     

         {/* ACCORDION */}
         <h2>Custom Accordions</h2>
         <Accordion
          accordion={expandedAccordion}
          activeKey={activeAccordion}
          onChange={key => setActiveAccordion(key)}>

          {accordionItems.map((item, idx) => {
            return <AccordionPanel
              key={idx.toString()}
              header={<>{item}</>}
              extra={'Some extra text'}>
                Some content
            </AccordionPanel>
          })}              
         
        </Accordion>

        <Divider />  

        {/* BUTTONS */}
        <h2>Custom Buttons</h2>
        <Button variant='ghost'>Button one ghost</Button>
        <Button variant='ghost' color='green'>Button two ghost</Button>
        <Button variant='ghost' color='textGrayDark'>Button three ghost</Button>
        <Button variant='ghost' color='red'>Button four ghost</Button>
        <Divider />  
        <Button variant='solid'>Button one solid</Button>
        <Button variant='solid' color='green'>Button two solid</Button>
        <Button variant='solid' color='textGrayDark'>Button three solid</Button>
        <Button variant='solid' color='red'>Button four solid</Button>
        <Divider />  
        <Button variant='ghost' mode='dark'>Button one ghost dark</Button>
        <Button variant='ghost' mode='dark' color='green'>Button two ghost dark</Button>
        <Button variant='ghost' mode='dark' color='textGrayDark'>Button three ghost dark</Button>
        <Button variant='ghost' mode='dark' color='red'>Button four ghost dark</Button>
        <Divider />  
        <Button variant='solid' mode='dark'>Button one solid</Button>
        <Button variant='solid' mode='dark' color='green'>Button two solid dark</Button>
        <Button variant='solid' mode='dark' color='textGrayDark'>Button three solid dark</Button>
        <Button variant='solid' mode='dark' color='red'>Button four solid dark</Button>

        <Divider />  

        {/* BUTTONS */}
        <h2>Country Dropdown</h2>
        <CountryDropdown variant='dark'/>
        <CountryDropdown variant='light'/>

        <Divider />  

        {/* Floating action btn */}
        <h2>Floating Action Button</h2>
        <FloatingActionButton onClick={() => alert('We could do many things by clicking this btn.')} color='purple' icon={<TransactionOutlined style={{fontSize: 20}} />} label='Click me' />
    
        <Divider />  

        {/* Loading Skeleton */}
        <h2>Skeleton loader</h2>
        <Button variant='solid' color='green' onClick={() => setShowSkeletonLoader(!showSkeletonLoader)}>
          Click To See me!
          {showSkeletonLoader && <Skeleton />}
        </Button>
       
    </CommonComponentsWrapper>
}

const CommonComponentsWrapper = styled.div`
    padding: 50px;
    flex: 1 1 0%;
    width: 0px;
`
