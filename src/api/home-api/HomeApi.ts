import { BaseApi } from "../base-api";

class HomeApi extends BaseApi {
    public async getMovies() {
        const response = await this.axios.get(`/https://api.themoviedb.org/3/search/movie?api_key=980abab8dc1ec723f4550bb0a31f8963&query=/`, {
          headers: {
            Accept: 'application/json',
          },
          cancelToken: this.cancelToken,
        });
        return response.data;
      };
    
}

export default HomeApi;
