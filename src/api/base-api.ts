import axios, {AxiosInstance, AxiosRequestConfig, AxiosError, CancelTokenSource, CancelToken} from 'axios';
const tokenSources: {
  [key: string]: CancelTokenSource;
} = {};

export type ApiErrorType = {
  statusCode: number;
  errorCode: string;
  message: string;
};


async function onCancelRequest(config: AxiosRequestConfig) {
  // if cancelToken is defined, we will cancel previous request and create a new token for new request.
  if (config.cancelToken) {
    const key = config.url;
    let tokenSource = tokenSources[key];
    if (tokenSource && tokenSource.cancel) {
      tokenSource.cancel('Operation canceled due to new request');
    }
    tokenSource = axios.CancelToken.source();
    config.cancelToken = tokenSource.token;
    tokenSources[key] = tokenSource;
  }

  return config;
}

async function onError(error: AxiosError, axiosInstance: AxiosInstance, errorList?: ApiErrorType[]) {
  let throwError = true;
  // return valid/empty response and exit
  if (axios.isCancel(error)) {
    console.warn(error);
    return Promise.resolve({data: {isCancel: true}});
  }
  // Common error handling
  const {response} = error;
  if (response) {
    // Any other error.
    const {status, data} = response;
    /* let showGeneralError = true; */
    if (errorList && errorList.length) {
      const errorItem = errorList.filter(err => err.statusCode == status && err.errorCode == data.code).pop();
      if (errorItem) {
        throwError = false;
      }
    }
  }
  if (throwError) {
    throw error;
  }
}





export class BaseApi {
  axios: AxiosInstance;
  axiosWithoutAuth: AxiosInstance;
  cancelToken: CancelToken;

  constructor(baseURL?: string, errorList?: ApiErrorType[]) {
    this.axios = axios.create({baseURL});
    this.axios.interceptors.response.use(
      res => res,
      err => onError(err, this.axios, errorList),
    );
    this.axios.interceptors.request.use(
      onCancelRequest,
    );
    this.axiosWithoutAuth = axios.create({baseURL});
    this.cancelToken = axios.CancelToken.source().token;
  }
}
