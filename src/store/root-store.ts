import {initHomeStore, HomeStore} from '@/store/home';
import {initDraftjsStore, DraftJsStore} from '@/store/draftJsStore';
import {applySnapshot, Instance, getSnapshot, types} from 'mobx-state-tree';

// Allows debugging mobx-state-tree roots
import makeInspectable from 'mobx-devtools-mst';
import { useMemo } from 'react';


const RootStore = types.model({
  homeStore: HomeStore,
  draftJsStore: DraftJsStore,
}).actions(self => {
  let initialState = {};
  return {
    afterCreate: () => {
      initialState = getSnapshot(self);
    },
    reset: () => {
      applySnapshot(self, initialState);
    },
  };
});

export type RootStoreType = Instance<typeof RootStore>;

let store: RootStoreType;

export function resetStore() {
  store.reset();
}

export function initializeStore(snapshot = null) {
  const _store =
    store ??
    RootStore.create({
      homeStore: initHomeStore(),
      draftJsStore: initDraftjsStore(),
    });
  if (snapshot) {
    applySnapshot(_store, snapshot);
  }
  if (typeof window === 'undefined') {
    return _store;
  }
  if (!store) {
    store = _store;
  }
  makeInspectable(store);
  return store;
}

export function useStore(initialState): Instance<typeof RootStore> {
  return useMemo(() => initializeStore(initialState), [initialState]);
}
