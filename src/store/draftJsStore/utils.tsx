import { punctuationRegex } from '@/utils/const/regex';
import {ContentStateConverter, convertToHTML} from 'draft-convert';

export const contentStateToHtml: ContentStateConverter = convertToHTML({
  blockToHTML: (block: any) => {
    if (block.type === 'code-block') {
      return <pre />;
    }
    if (block.type === 'section') {
      return <div />;
    }
    if (block.type === 'unordered-list-item') {
      return <ol />;
    }
    if (block.type === 'article') {
      return <article />;
    }
  },
  entityToHTML: ({type, data}, originalText) => {
    if (type === 'LINK') {
      return <a href={data.url}>{originalText}</a>;
    }
    if (type === 'IMAGE') {
      return <img src={data.src} alt='img' />;
    }
    if (type === 'code') {
      return <pre>{originalText}</pre>;
    }
  },
});

export function countWords(text: string): number {
  return text
    .replace(punctuationRegex, '')
    .split(/\s+/)
    .filter(x => x.trim().length > 0).length;
}
export function removeLineBreaks(value: string) {
  return value.replace(/(\r\n|\n|\r)/gm, '');
}
export function escapeHtml(value: string) {
  return String(value).replace(/[&<>"'`=\/]/g, s => {
    // @ts-ignore
    return htmlEntityMap[s];
  });
}
export function safeHtmlString(value: string) {
  return escapeHtml(removeLineBreaks(value));
}
