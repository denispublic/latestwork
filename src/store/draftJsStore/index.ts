
import {EditorState, SelectionState} from 'draft-js';
import {cast,types} from 'mobx-state-tree';


export const DraftJsStore = types
  .model({
    newTextSelection: types.frozen({}),
    currentEditorState: types.frozen({}),
    aiTooltip: types.model({
      show: types.maybeNull(types.boolean),
      isLink: types.maybeNull(types.boolean),
      isLinkEdit: types.maybeNull(types.boolean),
      selectedText: types.maybeNull(types.string),
      left: types.maybeNull(types.number),
      right: types.maybeNull(types.number),
      top: types.maybeNull(types.number),
      bottom: types.maybeNull(types.number),
      height: types.maybeNull(types.number),
      width: types.maybeNull(types.number),
      selectionRangeStart: types.maybeNull(types.number),
      selectionRangeEnd: types.maybeNull(types.number),
      urlValue: types.maybeNull(types.string),
      textValue: types.maybeNull(types.string),
      updatedSelection: types.maybeNull(types.frozen()),
    }),
    contentFragment: types.maybeNull(types.string),
    isCopy: types.boolean,
  }).views(self => ({
    get formattedEditorState() {
      if (self?.currentEditorState) {
        return self?.currentEditorState;
      } else {
        return EditorState.createEmpty();
      }
    },

    get getCurrentEditorState() {
      return self.currentEditorState;
    },
  }))
  .actions(self => {
    const setCurrentEditorState = (editorState: EditorState) => self.currentEditorState = cast(editorState);
    const setNewTextSelection = (selection: SelectionState) => self.newTextSelection = cast(selection);
    const setAiTooltip = (tooltip: any) => self.aiTooltip = tooltip;
    const setIsCopy = (isCopy: boolean) => self.isCopy = isCopy;

    return {
      setCurrentEditorState,
      setNewTextSelection,
      setAiTooltip,
      setIsCopy,
    };
  })
 

export const initDraftjsStore = () => {
  return DraftJsStore.create({
    currentEditorState: EditorState.createEmpty(),
    aiTooltip: {
      show: false,
      isLink: false,
      isLinkEdit: false,
      selectedText: '',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      height: 0,
      width: 0,
      selectionRangeStart: 0,
      selectionRangeEnd: 0,
      urlValue: '',
      textValue: '',
      updatedSelection: null,
    },
    contentFragment: '<p></p><p>This is some dummy text, that could easiliy be dynamic and fetched from BE</P>',
    isCopy: false,
  });
};
