import {types, Instance} from 'mobx-state-tree';

const HomeModel = types.model({
  // home store model types
  isHomePage: types.maybeNull(types.boolean),
})

export const HomeStore = types.model({
  home: HomeModel,
}).actions(self => {
  // some homeStore actions..
  const setIsHomePage = (value: boolean) => self.home.isHomePage = value;

  return {
    setIsHomePage
  };
}).views(self => ({
  // some methods to fetch homeStoreData
  get getIsHome() {
    return self.home.isHomePage;
  }

}))

export type HomeStoreType = Instance<typeof HomeStore>;


export function initHomeStore() {
  return HomeStore.create({
    // initial values
    home: {
      isHomePage: true,
    }
  });
}
