import {findKeywordRegex} from '@/constants/regex';

export function maxIndex<T>(arr: T[]): number | undefined {
  if (arr.length === 0) {
    return undefined;
  }
  let maxIdx = 0;
  for (let idx = 1; idx < arr.length; ++idx) {
    if (arr[idx] > arr[maxIdx]) {
      maxIdx = idx;
    }
  }
  return maxIdx;
}

export function stringToArray(stringdata: string, Regexp: any = null, content: string = '') {
  let data = stringdata;
  if (Regexp) {
    data = stringdata.replace(Regexp, content);
  }
  let updateData = data.split(findKeywordRegex);
  updateData = updateData.filter(function(el) {
    return el != '';
  });
  updateData.forEach((element, key) => {
    updateData[key] = element.trim().toLowerCase();
  });
  return updateData.filter(Boolean);
}

export function getUniqueData(objectData, checkKey) {
  return objectData?.filter((data, index) => {
    return index === objectData?.findIndex(obj => {
      return obj[checkKey] === data[checkKey];
    });
  });
}
