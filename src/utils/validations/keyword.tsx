import {findKeywordRegex} from '@/constants/regex';

export const maxKeywords = 50;
export const keywordLength = 80;
export const wordsLength = 10;

export const validationErrorMessage = `Input field can have only ${wordsLength} words or ${keywordLength} characters.`;

export const keywordListValidation = value => {
  const errorKeywordList = [];
  if (value.length == 0) {
    return {isValid: false, errorKeywordList: errorKeywordList};
  } else {
    const currentKeywordList = value.split(findKeywordRegex).filter(Boolean);
    if (currentKeywordList.length > maxKeywords) {
      return {isValid: false, errorKeywordList: errorKeywordList};
    }
    const keywordmap = currentKeywordList.map(keyword => {
      const words = keyword.split(' ');
      if (keyword.length > keywordLength || words.length > wordsLength || keyword.trim() == '') {
        errorKeywordList.push(keyword);
        return false;
      }
    });
    return keywordmap.includes(false) ? {isValid: false, errorKeywordList: errorKeywordList} : {isValid: true, errorKeywordList: errorKeywordList};
  }
  return {isValid: true, errorKeywordList: errorKeywordList};
};
