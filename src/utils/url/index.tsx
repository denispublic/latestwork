import {sanitizeRegex} from '@/constants/regex';

export function getSingleUrlParam(name: string): string | undefined {
  const params = new URLSearchParams(location.search);
  return params.get(name);
}

export const getPathname = () => {
  return location.pathname;
};

export const sanitizeUrl = (url: string): string => {
  return url?.replace(sanitizeRegex, '');
};

export const getKeywordFromUrl = (url: string | string[]): string => {
  const countyCode = (url.length === 3 && typeof url[url.length - 1] !== 'undefined') ? url[url.length - 1] : '';
  let keywordIndex = url.length - 2;
  if (countyCode == '') {
    keywordIndex = url.length - 1;
  }
  return url[keywordIndex];
};

export const getCountryFromUrl = (url: string | string[]): string => {
  return (url.length === 3 && typeof url[url.length - 1] !== 'undefined') ? url[url.length - 1] : '';
};

export const getDomain = (hostName: string) => {
  let domain = hostName;
  if (domain) {
    domain = domain.replace('sc-domain:', '');
    domain = domain.replace(/^http(s?):\/\//g, '');
    domain = domain.split('/')[0];
    domain = domain.replace(/\//g, '');
    domain = domain.replace(/www./g, '');
  }
  return domain ? domain : '';
};
export const getSiteName = (hostName: string) => {
  let domain = hostName;
  if (domain) {
    domain = domain.replace('sc-domain:', '');
    domain = domain.replace(/^http(s?):\/\//g, '');
    domain = domain.split('/')[0];
    domain = domain.replace(/\//g, '');
    domain = domain.replace(/www./g, '');
    domain = domain.split('.')[0];
  }
  return domain ? domain : '';
};

export const getDomainWithSlash = (hostName: string) => {
  let domain = hostName;
  if (domain) {
    domain = domain.replace('sc-domain:', '');
    domain = domain.replace(/^http(s?):\/\//g, '');
    domain = domain.replace(/www./g, '');
  }
  return domain ? domain : '';
};

export const getHostnameAndDomain = (url: string) => {
  let result = url;
  if (result) {
    result = result.replace('sc-domain:', '');
    result = result.replace(/^http(s?):\/\//g, '');
    result = result.replace(/\//g, '');
  }
  return result ? result : '';
};


export const getProjectName = (property: string) => {
  return property ? (property.startsWith('sc-domain:') ? property.split('sc-domain:')[1] : property) : '';
};

export const updateParams = (queryParams, paramKey, paramValue) => {
  let params = '';
  Object.keys(queryParams).forEach(param => {
    if (param !== 'slug') {
      if (!params) {
        params = `${param}=${param == paramKey ? paramValue : queryParams[param]}`;
      } else {
        params += `&${param}=${param == paramKey ? paramValue : queryParams[param]}`;
      }
    }
  });
  return params;
};

export const removeProtocol = (url: string) => {
  let result = url;
  if (result) {
    result = result.replace('sc-domain:', '');
    result = result.replace(/^http(s?):\/\//g, '');
  }
  return result ? result : '';
};

export const getPathNameFromUrl = (url: string) => {
  if (url) {
    const path = new URL(url).pathname;
    return path.replace('/', '');
  } else {
    return '';
  }
};

export const getPageNameFromUrl = (url: string) => {
  if (url) {
    const path = new URL(url).pathname;
    return path.replaceAll('/', '');
  } else {
    return '';
  }
};
export const isKrPublicPage = () => {
  const checkUrl = location.pathname.match(/\/keyword\/keyword-researcher-public\/.*/g);
  return !!checkUrl;
};

export const isCrPublicPage = () => {
  const checkUrl = location.pathname.match(/\/content\/content-researcher\/overview-public\/.*/g);
  return !!checkUrl;
};

export const isCaPublicPage = () => {
  const checkUrl = location.pathname.match(/\/content\/onpage-audit\/detail-public\/.*/g);
  return !!checkUrl;
};
