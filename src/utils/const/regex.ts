import escapeRegExp from 'lodash/escapeRegExp';


const punctuation = '!"#$%&\'()*+,-./:;<=>?@[]^_`{|}~';

export const specialCharactersRegex = /[^a-zA-Z0-9 _\s]/; // matches letters, numbers, space and enter

export const findKeywordRegex = /[\n,]/;

export const tagsRegex = new RegExp('(https:\/\/|http:\/\/)', 'g');

export const keywordNewLineReg = /\n/g;

export const emailNewLineReg = /\n/g;

export const emailReg = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;

export const emailRegShare = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/g;


export const psiRegex = /\n/g;

export const sanitizeRegex = new RegExp(`^(?:https?:\/\/)?(?:www\.)?`, 'g');

export const cleanUrlRegex = new RegExp(`^(?:https?:\/\/|http:\/\/)?(?:www\.)?`, 'g');

export const punctuationRegex = new RegExp(`[${escapeRegExp(punctuation)}]`, 'g');

export const urlValidationRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/ig;
export const urlValidationRegex2 = /^([a-z][a-z0-9\*\-\.]*):\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*(?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:(?:[a-z0-9\-\.]|%[0-9a-f]{2})+|(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\]))(?::[0-9]+)?(?:[\/|\?](?:[\w#!:\.\?\+=&@!$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/ig;

export const phoneValidationRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
export const phoneValidationRegex2 = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;


export const multiWordBoundaryRegex =normalizedTerms=>{
  return new RegExp(String.raw`\b(?:${normalizedTerms})\b`, 'g');
};

export const multiWordRegex =normalizedTerms=>{
  return new RegExp(`(^|[^\\w])(${normalizedTerms})($|[^\\w])`, 'g');
};

export const highlightKeywordRegex =keyword=>{
  return new RegExp(`(^|[^\\w])(${keyword})($|[^\\w])`, 'g');
};
