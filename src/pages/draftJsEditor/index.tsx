import { DraftEditor } from "@/components/pages/draftEditor"
import Head from 'next/head';

const DraftJsEditor = () => {
    return <>
          <Head>
            <title>Draft JS Demo</title>
            </Head>
        <DraftEditor />
    </>
    }

export default DraftJsEditor;