import Head from 'next/head';
import {NextPage} from 'next';
import {useRouter} from 'next/router';
import { useEffect } from 'react';

const Home: NextPage = () => {
  const router = useRouter();

  useEffect(() => {
    router.push('/draftJsEditor');
  }, [router]);

  return (
    <>
      <Head>
        <title>Denis Latest Work</title>
      </Head>
    </>
  );
};


export default Home;
