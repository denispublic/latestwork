import NextApp, {AppProps} from 'next/app';
import Head from 'next/head';
import {appWithTranslation} from 'next-i18next';

import {useStore} from '@/store/root-store';
import {observer} from 'mobx-react-lite';

import Layout from '@/components/layout';

import 'antd/dist/antd.css';
import './app.scss';

// The following import prevents a Font Awesome icon server-side rendering bug,
// where the icons flash from a very large icon down to a properly sized one:
import '@fortawesome/fontawesome-svg-core/styles.css';
// Prevent fontawesome from adding its CSS since we did it manually above:
import {config} from '@fortawesome/fontawesome-svg-core';

config.autoAddCss = false;

type StaticComponents = {
  getInitialProps?: typeof NextApp.getInitialProps;
};


const App: React.FC<AppProps> & StaticComponents = observer(
  ({Component, pageProps}: AppProps) => {
    const store = useStore(pageProps.initialState);
    return (
      <>
        <Head>
          <link rel='icon' href='' sizes='32x32' />
          <title>Denis Latest Work</title>
        </Head>
        <Layout store={store} pathname={pageProps.pathname}>
          <Component {...pageProps} rootStore={store}/>
        </Layout>
      </>
    );
  },
);

App.getInitialProps = async appContext => {
  return await NextApp.getInitialProps(appContext);
};


export default appWithTranslation(App);
