import { CommonComponentsPage } from "@/components/pages/CommonComponentsPage";
import { NextPage } from "next";
import Head from 'next/head';

const CommonComponents: NextPage = () => {
    return <>
        <Head>
            <title>Custom Inputs Demo</title>
        </Head>
        <CommonComponentsPage />
    </>
}

export default CommonComponents;