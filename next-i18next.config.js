
module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
    localeDetection: false, // remove is you want nextjs to dectect and redirect user to each locale.
  },
}
